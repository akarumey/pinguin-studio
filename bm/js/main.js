$(document).ready(function(){
	//Плавный скролл
	$("#menu").on("click","a", function (event) {
		//отменяем стандартную обработку нажатия по ссылке
		event.preventDefault();

		//забираем идентификатор бока с атрибута href
		var id  = $(this).attr('href'),

		//узнаем высоту от начала страницы до блока на который ссылается якорь
			top = $(id).offset().top;
		
		//анимируем переход на расстояние - top за 1500 мс
		$('body,html').animate({scrollTop: top}, 1500);
	});

	//Параллакс
	$('#first_block').parallax();
	$('#design').parallax();


	//Инициализация анимации
	new WOW().init();

	//Vue
	Vue.use(VeeValidate);

	var form = new Vue({
		el: '#newForm',
		data:{
			email:'',
			show: false,
		},
		methods:{
			validateBeforeSubmit() {
				this.$validator.validateAll().then((result) => {
					if (result) {
					// eslint-disable-next-line
						axios.post('/send_contacts.php', {
							'email': this.email,
						}).then((res)=>{
							this.show=true;
							this.email='';
							Vue.nextTick(() => {
								this.errors.clear()
							})	
						}).catch((err)=>{
							console.log(er)
						});
						return this.show=true;
					}	
				});
			}
		}
	});
	var formSecong = new Vue({
		el: '#newFormSecond',
		data:{
			email:'',
			show: false,
		},
		methods:{
			validateBeforeSubmit() {
				this.$validator.validateAll().then((result) => {
					if (result) {
					// eslint-disable-next-line
						axios.post('/send_contacts.php', {
							'email': this.email,
						}).then((res)=>{
							this.show=true;
							this.email='';
							Vue.nextTick(() => {
								this.errors.clear()
							})	
						}).catch((err)=>{
							console.log(er)
						});
						return this.show=true;
					}	
				});
			}
		}
	});

	//Слайдер в Умном функционале

	 $('.slider-for').slick({
	  slidesToShow: 1,
	  slidesToScroll: 1,
	  arrows: false,
	  fade: true,
	  asNavFor: '.slider-nav'
	});
	$('.slider-nav').slick({
	  slidesToShow: 1,
	  slidesToScroll: 1,
	  asNavFor: '.slider-for',
	  dots: true,
	  focusOnSelect: true
	});

	//Слайдер в Повышаем конверсию
	$('.slider-for_second').slick({
	  slidesToShow: 1,
	  slidesToScroll: 1,
	  arrows: false,
	  fade: true,
	  asNavFor: '.slider-nav_second'
	});
	$('.slider-nav_second').slick({
	  slidesToShow: 1,
	  slidesToScroll: 1,
	  asNavFor: '.slider-for_second',
	  dots: true,
	  focusOnSelect: true
	});
	
});

