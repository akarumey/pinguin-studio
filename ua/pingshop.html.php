<?php include "config.php"; ?>
<!DOCTYPE html>
<html lang="ru">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Інтернет-магазин "Швидкий старт" від Pinguin-Studio за 9900 грн. и за 3 дні</title>
	<meta name="description" content="Замовити створення, розробка інтернет-магазину недорого, Швидкий старт - ціна Вас зацікавить 9900 грн, дізнатись деталі ☎ 068 728 89 89 ">
	<link rel="stylesheet" href="/pingshop/css/normalize.css">
	<link rel="stylesheet" href="/pingshop/css/font-awesome.min.css">
	<link rel="stylesheet" href="/pingshop/css/flexboxgrid.css">
	<link rel="stylesheet" href="/pingshop/css/slick.css">
	<link rel="stylesheet" href="/pingshop/css/animate.css">
	<link rel="stylesheet" href="/pingshop/css/main.css">

	<link href="https://fonts.googleapis.com/css?family=Playfair+Display:900i&amp;subset=cyrillic" rel="stylesheet">
		<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-117832829-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-117832829-1');
</script>
<!-- Google Analytics -->
<script>
	(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

	ga('create', 'UA-117832829-1', 'auto');
	ga('send', 'pageview');
</script>
<!-- End Google Analytics -->
</head>
<body id="index">
	<div class="first_block_wrapper">
		<div class="first_block animated-background" id="first_block">
			<header>
				<div class="container-fluid header_border">
					<div class="row flex_wrap align_center first_block_inner_nav">
						<div class="col-xs-7 col-sm-4 col-lg-3 ">
							<a href="/ua">
								<img src="/pingshop/img/logo.png" alt="" class="img-responsive">
							</a>
						</div>
						<div class="col-xs-4 col-sm-3 col-lg-6 no_padding_right navigation">
							<nav id="nav_menu">  
								<div class="no_margin" v-on:click="showNav">
									<p class="tablet_menu_text">Меню</p>
									<button class="ham close_menu" v-bind:class="{ opened: isActive }">
										<span class="bar"></span>
										<span class="bar"></span>
										<span class="bar"></span>
									</button>
								</div>
								<transition name="fade">
									<ul class="flex flex_wrap nav_list" id="menu" v-if="show">
										<li><a href="#obzor">ОГЛЯД</a></li>
										<li><a href="#design">Дизайн</a></li>
										<li><a href="#functional">Функціонал</a></li>
										<li><a href="#demomodal" class="demomodal" data-modal="#demomodal">Демо</a></li>
										<li class="nav_list_last_item"><a href="#myModalCall" data-modal="#myModalCall" class="call_back">ЗАМОВИТИ ДЗВІНОК</a></li>
									</ul>
								</transition>    
							</nav>
						</div>
						<div class="col-sm-3 flex justify_content_end call_back_wrap">
							<a href="#buy" data-modal="#buy" class="call_back">ЗАМОВИТИ ДЗВІНОК</a>
						</div>
					</div>
				</div>
			</header>
			<div class="container-fluid first_block_inner">
				<div class="row">
					<div class="col-xs-12 col-lg-7">
						<div class="row no_margin">
							<div class="col-xs-12"></div>
							<div class="first_head wow slideInUp">
								<p class="first_bold_text">ШВИДКИЙ СТАРТ!</p>
								<p class="first_italic_text">Інтернет-магазин за 3 дні!</p>
								<p class="first_italic_text">Всього за 9900 грн.!</p>
							</div>
							<p class="first_text_plus wow slideInUp">
								Плюс хостинг та домен на 1 рік в подарунок!
							</p>
						</div>
						<div class="row no_margin text_center_mobile">
							<div class="col-xs-12 col-sm-4 col-lg-12">
                                <a href="#buy" class="call_back big bubble left wow slideInUp">Оформити замовлення</a>
								<div id="buy" class="modalDialog">
									<div>
										<a href="#close" title="Закрити" class="close">X</a>
<!--										<form class="data">-->
<!--											<input type="url" placeholder="Домен" required>-->
<!--											<input type="email" placeholder="Email" required>-->
<!--											<input class="submit-paypal call_back big bubble left wow" type="submit" value="--><?php //echo PAYPAL_BUTTON; ?><!--">-->
<!--                                            <input class="submit-yandex call_back big bubble left wow" type="submit" value="--><?php //echo YANDEX_BUTTON; ?><!--">-->
<!--										</form>-->
<!--										<form class="paypal hidden" action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top" style="display: inline-block">-->
<!--											<input type="hidden" name="cmd" value="_s-xclick">-->
<!--											<input type="hidden" name="encrypted" value="--><?php //echo PAYPAL_ENCRYPTED; ?><!--">-->
<!--											<input type="image" class="submit call_back big bubble left wow" border="0" name="submit" alt="Оплатить" value="Оплатить">-->
<!--											<img alt="" border="0" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" width="1" height="1">-->
<!--										</form>-->
<!--                                        <form class="yandex hidden" method="POST" action="https://money.yandex.ru/quickpay/confirm.xml">-->
<!--                                            <input type="hidden" name="receiver" value="--><?php //echo YANDEX_TRANSACTION_RECEIVER; ?><!--">-->
<!--                                            <input type="hidden" name="formcomment" value="--><?php //echo YANDEX_TRANSACTION_NAME; ?><!--">-->
<!--                                            <input type="hidden" name="short-dest" value="--><?php //echo YANDEX_TRANSACTION_APPOINTMENT; ?><!--">-->
<!--                                            <input type="hidden" name="targets" value="--><?php //echo YANDEX_TRANSACTION_APPOINTMENT; ?><!--">-->
<!--                                            <input type="hidden" name="quickpay-form" value="donate">-->
<!--                                            <input type="hidden" name="sum" value="--><?php //echo YANDEX_TRANSACTION_SUM; ?><!--" data-type="number">-->
<!--                                            <input type="hidden" name="need-fio" value="true">-->
<!--                                            <input type="hidden" name="need-email" value="true">-->
<!--                                            <input type="hidden" name="need-phone" value="true">-->
<!--                                            <input type="hidden" name="need-address" value="false">-->
<!--                                            <input type="submit" value="Перевести">-->
<!--                                        </form>-->
<!--                                        <img alt="" class="modal_logo" data-pagespeed-url-hash="4123841404" src="img/modal_2_top.png" onerror="this.onerror=null;pagespeed.lazyLoadImages.loadIfVisibleAndMaybeBeacon(this);">-->
<!--                                        <h2 class="modal_head">Закажите обратный звонок</h2>-->
<!--                                        <p class="modal_text">и мы свяжемся с Вами в ближайшее время</p>-->
<!--                                        <form class="fofmCall" action="" enctype="multipart/form-data">-->
<!--                                            <div class="modal-content_items">-->
<!--                                                <input type="text" required="" placeholder="Имя" name="txtname" class="modal_border">-->
<!--                                                <input type="tel" required="" placeholder="Телефон" name="txtphone" class="modal_border" id="modal_tel_call">-->
<!--                                            </div>-->
<!--                                            <span class="discuss_us_modal discuss_us flex align_center"><input type="submit" class="discuss_us_input" value="Замовити дзвінок" name="btnsend"></span>-->
<!---->
<!--                                            <p class="success_call_form"></p>-->
<!--                                        </form>-->
                                        <img alt="" class="modal_logo" data-pagespeed-url-hash="4123841404" src="img/modal_2_top.png" onerror="this.onerror=null;pagespeed.lazyLoadImages.loadIfVisibleAndMaybeBeacon(this);">
                                        <h2 class="modal_head">Замовити зворотній звя'зок</h2>
                                        <p class="modal_text">і ми зв'яжемось з Вами в найближчий час</p>
                                        <form class="fofmCall" action="" enctype="multipart/form-data">
                                            <div class="modal-content_items">
                                                <input type="text" required="" placeholder="Ім'я" name="txtname" class="modal_border">
                                                <input type="tel" required="" placeholder="Телефон" name="txtphone" class="modal_border" id="modal_tel_call">
                                            </div>
                                            <span class="discuss_us_modal discuss_us flex align_center"><input type="submit" class="discuss_us_input" value="Замовити дзвінок" name="btnsend"></span>

                                            <div class="success_call_form"></div>
                                        </form>
									</div>
								</div>
								<a href="https://hotshop.pinguin-studio.com.ua/" target="_blank" class="demomodal call_back transparent bubble left wow slideInUp">Дивитись демо</a>
							</div>
							<div class="col-xs-12 col-sm-8">
								<div id="shop_effects_mobile">
									<img src="/pingshop/img/new_img/devices_tablet.png" alt="" class="img-responsive mac_img">
								</div>
							</div>
						</div>

									 
					</div>
					<div id="shop_effects" class="shop_effect">
						<div class="shop_effect_one shop_effect layer" data-depth="0.2">
							<img src="/pingshop/img/new_img/phone_air.png" alt="" class="img-responsive mac_img">
						</div>
						<div class="shop_effect_two shop_effect layer" data-depth="0.4">
							<img src="/pingshop/img/new_img/ipad_air.png" alt="" class="img-responsive mac_img">
						</div>
						<div class=" shop_effect_three shop_effect layer" data-depth="0.6">
							<img src="/pingshop/img/new_img/mac_air.png" alt="" class="img-responsive mac_img">
						</div>
					</div>
				</div>
			</div>
		</div>
		<a href="#second_block" class="see_more"><img src="/pingshop/img/new_img/discuss_icon.png" alt=""></a>
	</div>
	<div class="second_block" id="second_block">
		<div class="container-fluid">
			<div class="row">
				<div class="col-xs-12">
					<div class="profi_wrap wow slideInUp">
						<p class="profi">Ця пропозиція <span class="profi_bold">для Вас якщо:</span></p>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12 col-sm-6 col-lg-5 second_block_item wow slideInUp">
					<div class="row no_margin">
						<div class="col-xs-12 col-sm-3 col-md-3 no_padding second_block_inner_img">
							<img src="/pingshop/img/new_img/second_block_1.png" alt="" class="img">
						</div>
						<div class="col-xs-12 col-sm-9 col-md-9 no_padding_right">
							<h3 class="second_block_headings">Ви відкриваєте свій перший Інтернет-магазин?</h3>
							<p class="second_block_text">Не наступайте на граблі. Використовуйте перевірене рішення.</p>
						</div>
					</div>
				</div>
				<div class="col-xs-12 col-sm-6 col-lg-5 col-lg-offset-1 second_block_item wow slideInUp">
					<div class="row no_margin">
						<div class="col-xs-12 col-sm-3 col-md-3 no_padding second_block_inner_img">
							<img src="/pingshop/img/new_img/second_block_2.png" alt="" class="img">
						</div>
						<div class="col-xs-12 col-sm-9 col-md-9 no_padding_right">
							<h3 class="second_block_headings">Ви хочете оновити свій Інтернет-магазин?</h3>
							<p class="second_block_text">Ми допоможемо перенести товари в новий магазин.</p>
						</div>
					</div>
				</div>
				<div class="col-xs-12 col-sm-5 second_block_item last wow slideInUp">
					<div class="row no_margin">
						<div class="col-xs-12 col-sm-3 col-md-3 no_padding second_block_inner_img">
							<img src="/pingshop/img/new_img/second_block_3.png" alt="" class="img">
						</div>
						<div class="col-xs-12 col-sm-9 col-md-9 no_padding_right">
							<h3 class="second_block_headings">Продаєте на торгових майданчиках Prom, zakupka?</h3>
							<p class="second_block_text">Відкрийте власний магазин і керуйте прибутком за своїм бажанням!</p>
						</div>
					</div>
				</div>
				<div class="col-xs-12 col-sm-5 col-sm-offset-1 second_block_item last wow slideInUp">
					<div class="row no_margin">
						<div class="col-xs-12 col-sm-3 col-md-3 no_padding second_block_inner_img">
							<img src="/pingshop/img/new_img/second_block_4.png" alt="" class="img">
						</div>
						<div class="col-xs-12 col-sm-9 col-md-9 no_padding_right">
							<h3 class="second_block_headings">У Вас є роздрібна точка або магазин?</h3>
							<p class="second_block_text">Залучайте масово клієнтів з інтернет!</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="third_block">
		<div class="container-fluid">
			<div class="row">
				<div class="col-xs-12 wow slideInUp">
					<div class="profi_wrap">
						<p class="profi">За 3 дні ви отримаєте</p>
						<p class="profi_bold">Інтернет-магазин, який продає.</p>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12 col-sm-4 third_block_item first wow slideInUp">
					<div class="row">
						<div class="col-xs-3 col-sm-3 text_center">
							<span class="play_fair">1</span>
						</div>
						<div class="col-xs-9 col-sm-9">
							<h3 class="second_block_headings">Адаптивний <br> дизайн </h3>
							<p class="second_block_text">для мобільних пристроїв</p>
						</div>
					</div>
				</div>
				<div class="col-xs-12 col-sm-4 third_block_item second wow slideInUp">
					<div class="row">
						<div class="col-xs-3 col-sm-3 text_center">
							<span class="play_fair">2</span>
						</div>
						<div class="col-xs-9 col-sm-9">
							<h3 class="second_block_headings">Потужний <br> функціонал </h3>
							<p class="second_block_text">для збільшення продажів</p>
						</div>
					</div>
				</div>
				<div class="col-xs-12 col-sm-4 third_block_item third wow slideInUp">
					<div class="row">
						<div class="col-xs-3 col-sm-3 text_center">
							<span class="play_fair">3</span>
						</div>
						<div class="col-xs-9 col-sm-9">
							<h3 class="second_block_headings">Зручне <br> наповнення </h3>
							<p class="second_block_text">і вигрузка товарів</p>
						</div>
					</div>
				</div>
				<div class="col-xs-12 col-sm-4 third_block_item fourth wow slideInUp">
					<div class="row">
						<div class="col-xs-3 col-sm-3 text_center">
							<span class="play_fair">4</span>
						</div>
						<div class="col-xs-9 col-sm-9">
							<h3 class="second_block_headings">Підготовлений <br> до просування </h3>
							<p class="second_block_text">в пошукових системах (Google, Yandex)</p>
						</div>
					</div>
				</div>
				<div class="col-xs-12 col-sm-4 third_block_item fifth wow slideInUp">
					<div class="row">
						<div class="col-xs-3 col-sm-3 text_center">
							<span class="play_fair">5</span>
						</div>
						<div class="col-xs-9 col-sm-9">
							<h3 class="second_block_headings">Проста <br> оплата  товару </h3>
							<p class="second_block_text">Приват24, PayPal і інші</p>
						</div>
					</div>
				</div>
				<div class="col-xs-12 col-sm-4 third_block_item sixth wow slideInUp">
					<div class="row">
						<div class="col-xs-3 col-sm-3 text_center">
							<span class="play_fair">6</span>
						</div>
						<div class="col-xs-9 col-sm-9">
							<h3 class="second_block_headings">Розміщення <br> в Інтернеті </h3>
							<p class="second_block_text">на швидкісному <br>  SSD-хостингу</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="fourth_block">
		<div class="container-fluid">
			<div class="row align_center">
				<div class="col-xs-12 col-lg-6 fourth_block_item">
					<h3 class="fourth_block_head wow slideInUp">Хочете збільшити середній чек і прибуток в кілька разів? </h3>
					<h4 class="fourth_block_head_bold wow slideInUp">Відправте заявку!</h4>
				</div>
				<div class="col-xs-12 col-lg-6 fourth_block_item">
					
					<div class="columns is-multiline" id="newForm">
						<div class="newForm_inner">
							<form action="POST" @submit.prevent='validateBeforeSubmit' class="flex">
								<div class="column is-12">
									<p class="control has-icon has-icon-right">
											<input name="email" v-model="email" v-validate.initial="'required|email'" :class="{'input': true, 'is-danger': errors.has('email') }" type="email" placeholder="Ваш Email..." autocomplete="off">
											<span class="error_wrap" style="display: none;">
												<i v-show="errors.has('email')" class="fa fa-warning"></i>
												<span v-show="errors.has('email')" class="help is-danger">
													Обов'язкове поле.
												</span>
											</span>
									</p>
								</div>
								<button>Відправити</button>
							</form>
						</div>
						<div class="resultPlace" style="display: none;" v-if="show">
							<p class="result">Дякуємо, дані відправлені! <i class="fa fa-check" aria-hidden="true"></i></p>	
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="fifth_block text-center" id="design">
		<div id="shop_effects_fourth" class="shop_effect_fourth">
			<div class="shop_effect_one_fifth shop_effect layer" data-depth="0.2">
				<img src="/pingshop/img/new_img/fifth_block_imac.png" alt="" class="img-responsive mac_img">
			</div>
			<div class="shop_effect_two_fifth shop_effect layer" data-depth="0.3">
				<img src="/pingshop/img/new_img/fifth_block_logo.png" alt="" class="img-responsive mac_img">
			</div>
			<div class=" shop_effect_three_fifth shop_effect layer" data-depth="0.4">
				<img src="/pingshop/img/new_img/fifth_block_menu.png" alt="" class="img-responsive mac_img">
			</div>
			<div class="shop_effect_four_fifth shop_effect layer" data-depth="0.5">
				<img src="/pingshop/img/new_img/fifth_block_screen.png" alt="" class="img-responsive mac_img">
			</div>
			<div class=" shop_effect_five_fifth shop_effect layer" data-depth="0.6">
				<img src="/pingshop/img/new_img/fifth_block_iPhone.png" alt="" class="img-responsive mac_img">
			</div>
		</div>
		<div class="container-fluid">
			<div class="row">
				<div class="col-xs-12 col-sm-5 col-sm-offset-7 col-lg-7 col-lg-offset-5 fifth_block_colums">
					<h3 class="some_about_us wow slideInUp">
						АДАПТИВНИЙ ДИЗАЙН
					</h3>
					<p class="some_about_us_text wow slideInUp">
						До 60% відвідувачів заходять в Інтернет-магазин через мобільні пристрої! Якщо Ваш сайт немає мобільної версії, ви втрачаєте клієнтів, продажу, прибуток.
					</p>
					<ul class="fifth_block_list wow slideInUp">
						<li>
							<p>- 4 розширення екрану: 
							<span>1920, 1280, 750, 320 px</span>
							</p>
						</li>
						<li><p>- "движок" - популярна система управління PrestaShop</p></li>
						<li><p>- 15 зверстаних сторінок</p></li>
						<li><p>- самостійний вибір кольорової гами</p></li>
					</ul>
					<a href="https://hotshop.pinguin-studio.com.ua/" target="_blank" class="demomodal call_back big wow slideInUp">
						Дивитись демо
					</a>
				</div>

			</div>
		</div>
	</div>
	<div class="sixth_block six" id="functional">
		<div class="sixth_block_inner">
			<div class="container-fluid">
				<div class="row">
					<div class="col-xs-12 col-lg-5 fifth_block_colums">
						<div class="sixth_block_head">
							<h3 class="some_about_us wow slideInUp">
								Розумний функціонал
							</h3>
							<p class="some_about_us_text wow slideInUp">
								Більше 40 найбільш затребуваних модулів для інтернет-магазинів
							</p>
						</div>
						<div class="slider-nav wow slideInUp">
							<div class="slider_nav_first">
								<h4 class="some_about_us">Модуль “Разом дешевше”</h4>
								<p class="some_about_us_text">Можливість формувати групову покупку товарів зі знижкою. Зручне управління блоків, можливість формувати необмежену кількість пакетів.</p>
							</div>
							<div class="slider_nav_second">
								<h4 class="some_about_us">Модуль "Блог"</h4>
								<p class="some_about_us_text">Повноцінний блог статтей або новин з керуванням і СЕО налаштуванням. Вивід останніх записів на головну у вигляді слайдера.</p>
							</div>
							<div class="slider_nav_three">
								<h4 class="some_about_us">Модуль “Фільтри”</h4>
								<p class="some_about_us_text">Повноцінна фільтрація по будь-яким характеристикам товарів. Повністю автоматизовані і налаштовані фільтри товарів. Ajax підвантаження результатів фільтрації і сортування.</p>
							</div>
							<div class="slider_nav_four">
								<h4 class="some_about_us">Повноцінна картка товару</h4>
								<p class="some_about_us_text">Налаштована картка товару з характеристиками, керованими відгуками,  атрибутами, аксесуарами, купівлею в 1 клік і статичної інформацією.</p>
							</div>
							<div class="slider_nav_five">
								<h4 class="some_about_us">Особистий кабінет</h4>
								<p class="some_about_us_text">Розширений особистий кабінет з історією замовлень, адресами, списком бажань, обміном повідомлень і редагуванням особистої інфорамаціі.</p>
							</div>
							<div class="slider_nav_six">
								<h4 class="some_about_us">Модуль "Розширене меню"</h4>
								<p class="some_about_us_text">Розширене горизонтальне меню, що повністю налаштовується з можливістю виведення і розташування будь-якої інформації з магазину.</p>
							</div>
							<div class="slider_nav_seven">
								<h4 class="some_about_us">Зручне оформлення замовлення</h4>
								<p class="some_about_us_text">Зручне зрозуміле односторінкове оформлення замовлення з можливістю вибору відділень Нової Пошти та онлайн оплатою. Безліч налаштувань з управління оформленням.</p>
							</div>
						</div>
						
					</div>
					<div class="col-xs-12 col-lg-7 slider-for_wrap">
						<div class="slider-for wow slideInUp">
							<div>
								<img src="/pingshop/img/new_img/sixth_slide_1.jpg" alt="">
							</div>
							<div>
								<img src="/pingshop/img/new_img/pr2.jpg" alt="">
							</div>
							<div>
								<img src="/pingshop/img/new_img/pr3.jpg" alt="">
							</div>
							<div>
								<img src="/pingshop/img/new_img/pr4.jpg" alt="">
							</div>
							<div>
								<img src="/pingshop/img/new_img/pr5.jpg" alt="">
							</div>
							<div>
								<img src="/pingshop/img/new_img/pr6.jpg" alt="">
							</div>
							<div>
								<img src="/pingshop/img/new_img/pr7.jpg" alt="">
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="seventh_block seven">
		<div class="seventh_block_inner">
			<div class="container-fluid">
				<div class="row">
					<div class="col-xs-12 col-lg-7 wow slideInUp slider-for_wrap">
						<div class="slider-for_second">
							<div>
								<img src="/pingshop/img/new_img/pr11.jpg" alt="">
							</div>
							<div>
								<img src="/pingshop/img/new_img/pr14.jpg" alt="">
							</div>
							<div>
								<img src="/pingshop/img/new_img/pr10.jpg" alt="">
							</div>
							<div>
								<img src="/pingshop/img/new_img/pr12.jpg" alt="">
							</div>
							<div>
								<img src="/pingshop/img/new_img/pr15.jpg" alt="">
							</div>
							<div>
								<img src="/pingshop/img/new_img/pr9.jpg" alt="">
							</div>
						</div>
					</div>
					<div class="col-xs-12 col-lg-5 fifth_block_colums">
						<div class="sixth_block_head">
							<h3 class="some_about_us wow slideInUp">
								СЕРВІСИ І SEO
							</h3>
							<p class="some_about_us_text wow slideInUp">
								Впроваджені популярні зовнішні сервіси. Проведена підготовка до SEO просування.
							</p>
						</div>
						<div class="slider-nav_second wow slideInUp">
							<div class="slider_nav_first">
								<h4 class="some_about_us">Синхронізація з Прайсагрегаторами</h4>
								<p class="some_about_us_text">Впроваджено модуль формування YML файлу для відправки в популярні сервіси Prom.ua, Hotline.ua і т.д. Має багато налаштувань для формування потрібного експорту</p>
							</div>
							<div class="slider_nav_second wow slideInUp">
								<h4 class="some_about_us">Модуль "Новая Почта"</h4>
								<p class="some_about_us_text">Впроваджено модуль Нової Пошти при оформленні замовлення. Тепер Ваші клієнти можуть вибирати місто і відділення з бази Нової Пошти при оформленні замовлення.</p>
							</div>
							<div class="slider_nav_second wow slideInUp">
								<h4 class="some_about_us">Модулі платіжних сервісів</h4>
								<p class="some_about_us_text">Впроваджено відразу 4 популярних платіжних сервісу, а також можливість безготівкової оплати та оплати за фактом отримання товару. Всі модулі налаштовуються.</p>
							</div>
							<div class="slider_nav_three wow slideInUp">
								<h4 class="some_about_us">СЕО магазину</h4>
								<p class="some_about_us_text">Передбачена можливість повного СЕО налаштування всіх сторінок, товарів, категорій та інших елементів, а повний доступ до файлів магазина надає можливість тонкого налаштування для сео-спеціаліста.</p>
							</div>
							<div class="slider_nav_four wow slideInUp">
								<h4 class="some_about_us">Модуль "Карта сайту"</h4>
								<p class="some_about_us_text">Автоматична генерація карти сайту для пошукових систем. Можливість налаштування генератора із зазначенням генерації потрібних сторінок.</p>
							</div>
							<div class="slider_nav_four wow slideInUp">
								<h4 class="some_about_us">Модуль "Вхід через соцмережі"</h4>
								<p class="some_about_us_text">Надається можливість реєстрації клієнтів через популярні соціальні сервіси - Google+ і Facebook. Спрощує процедуру входу клієнта.</p>
							</div>
						</div>
						
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="fourth_block">
		<div class="container-fluid">
			<div class="row align_center">
				<div class="col-xs-12 col-lg-6 fourth_block_item">
					<h3 class="fourth_block_head wow slideInUp">Хотите увеличить средний чек и прибыль в несколько раз? </h3>
					<h4 class="fourth_block_head_bold wow slideInUp">Отправьте заявку!</h4>
				</div>
				<div class="col-xs-12 col-lg-6 fourth_block_item">
					
					<div class="columns is-multiline" id="newFormSecond">
						<div class="newForm_inner">
							<form action="POST" @submit.prevent='validateBeforeSubmit' class="flex">
								<div class="column is-12">
									<p class="control has-icon has-icon-right">
											<input name="email" v-model="email" v-validate.initial="'required|email'" :class="{'input': true, 'is-danger': errors.has('email') }" type="email" placeholder="Ваш Email..." autocomplete="off">
											<span class="error_wrap" style="display: none;">
												<i v-show="errors.has('email')" class="fa fa-warning"></i>
												<span v-show="errors.has('email')" class="help is-danger">
													Обязательное поле.
												</span>
											</span>
									</p>
								</div>
								<button>Відправити</button>
							</form>
						</div>
						<div class="resultPlace" v-if="show" style="display: none;">
							<p class="result">Спасибо, данные отправлены! <i class="fa fa-check" aria-hidden="true"></i></p>	
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="sixth_block eight_block">
		<div class="sixth_block_inner">
			<div class="container-fluid">
				<div class="row">
					<div class="col-xs-12 col-lg-5 fifth_block_colums">
						<div class="sixth_block_head">
							<h3 class="some_about_us wow slideInUp">
								РОЗШИРЕНА АНАЛІТИКА
							</h3>
							<p class="some_about_us_text wow slideInUp">
								Якщо Ваш бізнес вимірюється, то ним можна керувати.
							</p>
							<p class="some_about_us_text center wow slideInUp">
								Надається вбудована в Ваш інтернет-магазин аналітика, а також можливість рахувати прибуток. Додатково впроваджується аналітика Google Analytics, щоб Ви могли стежити і аналізувати ефективність продажів Вашого інтернет-магазину.
							</p>
						</div>					
					</div>
					<div class="col-xs-12 col-lg-7 wow slideInUp sixth_block_inner_img">
						<img src="/pingshop/img/new_img/eight_block_img.jpg" alt="" class="eigth_block_img">
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="seventh_block nineth_block">
		<div class="seventh_block_inner">
			<div class="container-fluid">
				<div class="row">
					<div class="col-xs-12 col-lg-7 wow slideInUp sixth_block_inner_img">
						<img src="/pingshop/img/new_img/nineth_block_img.jpg" alt="" class="eigth_block_img">
					</div>
					<div class="col-xs-12 col-lg-5 fifth_block_colums">
						<div class="sixth_block_head">
							<h3 class="some_about_us wow slideInUp">
								ЗРУЧНА АДМИН-ПАНЕЛЬ
							</h3>
							<p class="some_about_us_text wow slideInUp">
								Керуйте своїм бізнесом з будь-якої точки світу з будь-якого пристрою.
							</p>

							<p class="some_about_us_text center wow slideInUp">
								Зручна адмін-панель дозволяє здійснювати всі необхідні дії по роботі з сайтом як Вам, так і Вашим менеджерам: управління товарами, категоріями, управління клієнтами і замовленнями. Всі дані надійно зберігаються з логуванням дій кожного співробітника.
							</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="sixth_block tenth_block">
		<div class="sixth_block_inner">
			<div class="container-fluid">
				<div class="row">
					<div class="col-xs-12 text_center">
						<h3 class="fourth_block_head wow slideInUp">
							Чому Вам вигідно користуватися
						</h3>
						<h4 class="fourth_block_head_bold wow slideInUp">
							Швидким стартом?
						</h4>
					</div>
					<div class="col-xs-12 col-sm-4 third_block_item wow slideInUp">
						<div class="row">
							<div class="col-xs-3 col-sm-4 col-lg-3 text_center no_padding_right wow slideInUp">
								<img src="/pingshop/img/new_img/ten_block_1.png" alt="">
							</div>
							<div class="col-xs-9 col-sm-8 col-lg-9 no_padding_right">
								<h3 class="second_block_headings wow slideInUp">ШВИДКО</h3>
								<p class="second_block_text wow slideInUp">
									3 дні - і Ваш магазин відкритий. Без виснажливих узгоджень
								</p>
							</div>
						</div>
					</div>
					<div class="col-xs-12 col-sm-4 third_block_item wow slideInUp">
						<div class="row">
							<div class="col-xs-3 col-sm-4 col-lg-3 text_center no_padding_right wow slideInUp">
								<img src="/pingshop/img/new_img/ten_block_2.png" alt="">
							</div>
							<div class="col-xs-9 col-sm-8 col-lg-9 no_padding_right">
								<h3 class="second_block_headings wow slideInUp">ЗРУЧНО</h3>
								<p class="second_block_text wow slideInUp">
									Зручно для Вас і зручно для покупця
								</p>
							</div>
						</div>
					</div>
					<div class="col-xs-12 col-sm-4 third_block_item wow slideInUp">
						<div class="row">
							<div class="col-xs-3 col-sm-4 col-lg-3 text_center no_padding_right wow slideInUp">
								<img src="/pingshop/img/new_img/ten_block_3.png" alt="">
							</div>
							<div class="col-xs-9 col-sm-8 col-lg-9 no_padding_right">
								<h3 class="second_block_headings wow slideInUp">ПРОСТО</h3>
								<p class="second_block_text wow slideInUp">
									Не потрібно знати програмування або розбиратися в складних питаннях.
								</p>
							</div>
						</div>
					</div>
					<div class="col-xs-12 col-sm-4 third_block_item wow slideInUp">
						<div class="row">
							<div class="col-xs-3 col-sm-4 col-lg-3 text_center no_padding_right wow slideInUp">
								<img src="/pingshop/img/new_img/ten_block_4.png" alt="">
							</div>
							<div class="col-xs-9 col-sm-8 col-lg-9 no_padding_right">
								<h3 class="second_block_headings wow slideInUp">БЕЗПЕЧНО</h3>
								<p class="second_block_text wow slideInUp">
									Технічна підтримка, оптимізація для роботи з навантаженнями - все вже готово.
								</p>
							</div>
						</div>
					</div>
					<div class="col-xs-12 col-sm-4 third_block_item wow slideInUp">
						<div class="row">
							<div class="col-xs-3 col-sm-4 col-lg-3 text_center no_padding_right wow slideInUp">
								<img src="/pingshop/img/new_img/ten_block_5.png" alt="">
							</div>
							<div class="col-xs-9 col-sm-8 col-lg-9 no_padding_right">
								<h3 class="second_block_headings wow slideInUp">ПРИБУТКОВО</h3>
								<p class="second_block_text wow slideInUp">
									Швидкий старт продає: ми протестували його на сотнях клієнтів.
								</p>
							</div>
						</div>
					</div>
					<div class="col-xs-12 col-sm-4 third_block_item wow slideInUp">
						<div class="row">
							<div class="col-xs-3 col-sm-4 col-lg-3 text_center no_padding_right wow slideInUp">
								<img src="/pingshop/img/new_img/ten_block_6.png" alt="">
							</div>
							<div class="col-xs-9 col-sm-8 col-lg-9 no_padding_right">
								<h3 class="second_block_headings wow slideInUp">ВИГІДНО</h3>
								<p class="second_block_text wow slideInUp">
									Продажі при мінімальних вкладеннях!
								</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="eleventh_block">
		<h3 class="fourth_block_head text_center wow slideInUp">
			Як почати користуватися
		</h3>
		<h4 class="fourth_block_head_bold text_center wow slideInUp">
			Швидким стартом?
		</h4>

		<p class="text_center wow slideInUp">
			<a href="#buy1" class="call_back big ten">ПОЧАТИ КОРИСТУВАТИСЯ</a>
		</p>

		<div class="eleventh_block_item text_center wow slideInUp">
			<p class="second_block_text">Знайомимося з Вашим бізнесом. Укладаємо договір.</p>
			<p class="orange">1 день</p>
		</div>
		<div class="eleventh_block_item text_center wow slideInUp">
			<p class="second_block_text">Реєструємо домен, переносимо на хостинг</p>
			<p class="orange">1 день</p>
		</div>
		<div class="eleventh_block_item text_center wow slideInUp">
			<p class="second_block_text">Стверджуємо кольорову гаму, тестуємо, навчаємо.</p>
			<p class="orange">1 день</p>
		</div>
		<p class="text_center wow slideInUp">
			<a href="#buy1" class="call_back big last">Залишити заявку</a>
		</p>
        <div id="buy1" class="modalDialog">
            <div>
                <a href="#close" title="Закрыть" class="close">X</a>
                <img alt="" class="modal_logo" data-pagespeed-url-hash="4123841404" src="img/modal_2_top.png" onerror="this.onerror=null;pagespeed.lazyLoadImages.loadIfVisibleAndMaybeBeacon(this);">
                <h2 class="modal_head">Замовити зворотній звінок</h2>
                <p class="modal_text">і ми зв'яжемося з Вами в найближчий час</p>
                <form class="fofmCall" action="" enctype="multipart/form-data">
                    <div class="modal-content_items">
                        <input type="text" required="" placeholder="Ім'я" name="txtname" class="modal_border">
                        <input type="tel" required="" placeholder="Телефон" name="txtphone" class="modal_border" id="modal_tel_call">
                    </div>
                    <span class="discuss_us_modal discuss_us flex align_center"><input type="submit" class="discuss_us_input" value="Замовити дзвінок" name="btnsend"></span>

                    <p class="success_call_form"></p>
                </form>
            </div>
        </div>
	</div>
 <div id="demomodal" class="modalDialog">
        <div>
            <a href="#close" title="Закрити" class="close">X</a>
            <h2>Демо front-office:</h2>
            <a href="https://hotshop.pinguin-studio.com.ua/" target="_blank" id="see_front">Дивитись</a>
            <h2>Демо back-office:</h2>
            <div class="alert alert-info">
                <p>
                    <b>Логін: demo@demo.com</b></p><br/>
                </p>
                <p>
                    <b>Пароль: demodemo</b><br/>
                </p>
            </div>
            <a href="https://hotshop.pinguin-studio.com.ua/root" target="_blank" id="see_front">Дивитись</a>
        </div>
    </div>
	<footer>
		<div class="container-fluid">
			<div class="row">
				<div class="col-xs-12 col-sm-6 col-md-6 display_inline_block wow slideInUp">
					<h3 class="footer_head"><span>Контакти</span></h3>
					<div class="row footer_contacts_line">
						<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 no_padding">
							<p class="footer_phone">Телефон:</p>
						</div>
						<div class="col-xs-9 col-sm-9 col-md-9 col-lg-9 no_padding">
							<span><a href="tel:0687288989" class="footer_phone_item">(068) 728-89-89</a>, <a href="tel:0953397379">(095) 339-73-79</a></span>
						</div>
					</div>
					<div class="row footer_contacts_line">
						<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 no_padding">
							<p class="email">Mail:</p>
						</div>
						<div class="col-xs-9 col-sm-9 col-md-9 col-lg-9 no_padding">
						 	<span><a href="mailto:zakaz@pinguin-studio.com.ua" class="footer_phone_item">zakaz@pinguin-studio.com.ua</a></span>
						</div>
					</div>
					 <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 no_padding">
					 	<ul class="footer_social flex flex_wrap">
							<li><a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
							<li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
							<li><a href="#"><i class="fa fa-youtube" aria-hidden="true"></i></a></li>
							<li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
							<li><a href="#"><i class="fa fa-behance" aria-hidden="true"></i></a></li>
						</ul>
					 </div>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-6 display_inline_block wow slideInUp">
					<h3 class="footer_head"><span>Коротко про нас для роботів</span></h3>
					<p class="footer_text_for_robots">Pinguin-Studio.com.ua - Створення сайтів в Дніпрі. Розробка інтернет-магазинів PINGUIN-STUDIO. Створення сайтів в Дніпрі, розробка інтернет-магазинів в Дніпрі. Розробка сайтів, розкрутка, просування. © PINGUIN-STUDIO, 2009-2018</p>
				</div>	
			</div>
		</div>
	</footer>
<!-- Latest compiled and minified JavaScript -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="/pingshop/js/slick.min.js"></script>
    <script src="/pingshop/js/parallax.js"></script>
    <script src="/pingshop/js/wow.min.js"></script>
    <script src="/pingshop/js/vue.js"></script>
    <script src="/pingshop/js/vee-validate.js"></script>
    <script src="/pingshop/js/axios.min.js"></script>
    <script src="/pingshop/js/main.js"></script>
    <script src="/pingshop/js/buy.js"></script>
<!-- <script src="js/preloader_home.js"></script> -->
<!-- BEGIN JIVOSITE CODE {literal} -->
<script type='text/javascript'>
(function(){ var widget_id = 'KrP5X7jF78';var d=document;var w=window;function l(){
var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = '//code.jivosite.com/script/widget/'+widget_id; var ss = document.getElementsByTagName('script')[0]; ss.parentNode.insertBefore(s, ss);}if(d.readyState=='complete'){l();}else{if(w.attachEvent){w.attachEvent('onload',l);}else{w.addEventListener('load',l,false);}}})();</script>
<!-- {/literal} END JIVOSITE CODE -->
</body>
</html>