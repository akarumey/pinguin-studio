$(document).ready(function(){

	//Go TOP
    $('.go_top').on('click', function(){
        $('html, body').animate({ scrollTop:0 }, 1500);
    });

    $(window).scroll(function () {
        if ($(this).scrollTop() > 400) {
            $('.go_top_block').fadeIn();
        } else {
            $('.go_top_block').fadeOut();
        }
    });

    //Изменение фона и цвета у иконок раздела ВОЗМОЖНОСТИ
	function svg_hover(){
		$(this).hover(function(){
			$(this).parentsUntil('.second_block_item_wrap').find('.second_block_item_img').addClass('svg_hover');
		},
			function(){
				$(this).parentsUntil('.second_block_item_wrap').find('.second_block_item_img').removeClass('svg_hover');
		});
	}
	$('.second_block_headings').each(svg_hover);

	//Анимация блока с языками
	$("ul.lang-select").hover(function(){
		$(this).find(".hide_lang").removeClass('hide_lang').addClass('show_lang');
		},
		function(){
		  $(this).find(".show_lang").removeClass('show_lang').addClass('hide_lang');
	});
	//Добавляем слайдер на главную на мобильных

	if($(window).width() <= 767){
        $('.done_projects').addClass('regular');
    }

    //Инициализация слайдера

	$(".regular").slick({
		dots:false,
		slidesToShow:7,
		slidesToScroll:1,
		responsive:[{
			breakpoint:991,
			settings:{
				slidesToShow:3,
			}
		},
		{
			breakpoint:767,
			settings:{
				slidesToShow:1,
			}
		}]
	});

	//Параллакс на странице магазина

	$('#shop_effects').parallax()
	$('#shop_effects_second').parallax()

	

	
	//Инициализация анимации
	new WOW().init();
	
	//аякс форма обратной связи
	//проверяет какой ответ был получен
	//и в зависимости от ответа
	//выводит информацию о статусе
	//отправки письма

/*	$(".fofm").submit(function() {
		$.ajax({
			type: "POST",
			url: "contact.php",
			data: data,
			success: $('.success_form').text('Успешно отправлено!')
		});
		return false;
	});*/

	$('.fofm').on('submit', function(e){
		e.preventDefault();
		var $that = $(this),
		formData = new FormData($that.get(0)); // создаем новый экземпляр объекта и передаем ему нашу форму (*)
		$.ajax({
			url: "contact.php",
			type: "POST",
			contentType: false, // важно - убираем форматирование данных по умолчанию
			processData: false, // важно - убираем преобразование строк по умолчанию
			data: formData,
			dataType: 'json',
			success: function(r) {
				if(r == 'empty')
					alert('Заполните все поля');
				else
					$('.success_form').text('Успешно отправлено!')
            }
		});
	});

	$(".fofmCall").submit(function() {
		var str = $(this).serialize();
		$.ajax({
			type: "POST",
			url: "contact_call.php",
			data: str,
			success: function(r) {
                if (r == 'empty')
                    alert('Заполните все поля');
                else
                    $('.success_call_form').text('Успешно отправлено!')
            }
		});
		return false;
	});

	//Смена текста в модальном окне при выборе файла
	$('#modal_file').change(function(){

		$('#label_modal_file').text('Изменить файл');

		if ($(this).val().lastIndexOf('\\')) {
			var n = $(this).val().lastIndexOf('\\') + 1;
		} else {
			var n = $(this).val().lastIndexOf('/') + 1;
		}

		var fileName = $(this).val().slice(n);

		$("#loaded_file_name").text(fileName);

	});

	//Анимация гамбургера
	$('.close_menu').on('click', function() {
	  var $btn = $(this);
	  if ($btn.hasClass('closed') || !$btn.hasClass('opened')) {
	    $btn.removeClass('closed');
	    setTimeout(function() {
	      // Little hack in order to restart CSS animations ...
	      $btn.addClass('opened');
	    }, 1);
	  } else if ($btn.hasClass('opened')) {
	    $btn.removeClass('opened');
	    setTimeout(function() {
	      // Little hack in order to restart CSS animations ...
	      $btn.addClass('closed');
	    }, 1);
	  }
	});

	if($.cookie('modal')=='true') {
		
	} else {
		setTimeout(function(){
			$('.left-modal').addClass('active');
		},3000);
	}
	
	$('.left-modal .close-modal').click(function(e){
		e.preventDefault();
		if($(this).parent().hasClass('active')) {
            $(this).parent().removeClass('active');
			$.cookie('modal', 'true');
        }
	});
	
	
});

