$(document).ready(function(){
	var pc=0;get_portfolios();
	$('button.more').click(function(e){
		e.preventDefault();
		pc++;
		get_portfolios();
	});
	function get_portfolios(){
		$('.load').css('display','block');
		var tid='0';
		$('.portfolios').find('.portfolio_item').each(function(){
			tid=$(this).attr('id');
		});
		$.ajax({
			type:'POST',
			url:'/include/portfolio.php',
			data:'&count='+pc+'&id='+tid+'&ua=1',
			success:function(msg){
				console.log(msg);
				var data=JSON.parse(msg);
				for(var i=0;i<data.length;i++){
					if(data[i]['thumb']!=null){
						var html='<div id="';
						html+=data[i]['id'];
						html+='" class="col-xs-12 col-sm-4 col-md-4 col-lg-4 portfolio_item"><img src="';
						html+=data[i]['thumb'];
						html+='" alt="" class="img-responsive portfolio_item_img"><a href="';
						html+=data[i]['link'];
						html+='"><div class="inner_portfolio_wrap"><div class="inner_portfolio"><div class="protfolio_text_wrap"><p class="portfolio_head">';
						html+=data[i]['title'];
						html+='</p><p class="portfolio_text">';
						html+=data[i]['desc'];
						html+='</p></div></div></div></a></div>';
						$('.third_block_wrap.portfolios').append(html);
					}
				}
				if(data.length<6){
					$('button.more').fadeOut('slow',function(){
						$(this).css('display','block');
						$(this).css('opacity','0');});
				}
				$('.load').css('display','none');
			}
		});
	}
});