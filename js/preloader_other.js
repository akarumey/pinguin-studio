$(window).on('load', function () {
    var $preloader = $('#hellopreloader'),
        $svg_anm   = $preloader.find('#hellopreloader_preload');
    $svg_anm.fadeOut();
    $preloader.delay(500).fadeOut('slow');
});