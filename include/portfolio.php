<?php
include "simple_html_dom.php";

	if(isset($_POST["count"])) {
		$count = $_POST["count"];
		$folder = $_SERVER['DOCUMENT_ROOT'];
		if($_POST["ua"]==="1") {
			$folder = $_SERVER['DOCUMENT_ROOT']."ua/";
		}
		$list = scandir($folder);
		array_shift($list);
		array_shift($list);
		$portfolios_files = array();
		foreach ($list as $item) {
			if (mb_stripos($item, 'portfolio_', 0, 'utf-8') === 0) {
				array_push($portfolios_files, $item);
			}
		}
		$portfolios = array();

		$st = 0;
		if($count!=0) {
			if($count==1) {
				$st = 7;
			} else {
				$st = 6 * $count;
			}
		}
		$ex = $st+6;
		if(count($portfolios_files)<$ex) {
			$ex = count($portfolios_files);
		}
		$files_numbers = array();
		$tst = $st;
		if($tst!=(int)$_POST["id"]+1 && (int)$_POST["id"]!=0) {
			$tst=(int)$_POST["id"]+1;
		}
		for($i=0;$i<6;$i++) {
			array_push($files_numbers,$tst);
			$tst++;
		}
		for($i=0;$i<6;$i++) {
			$test = true;
			do {
				$test = true;
				if($files_numbers[$i]!=-1) {
					if($files_numbers[$i]<count($portfolios_files)) {
                        if($_POST["ua"]==="1") {
                            $html = file_get_html("https://pinguin-studio.com.ua/ua/" . $portfolios_files[$files_numbers[$i]]);
                        } else {
                            $html = file_get_html("https://pinguin-studio.com.ua/" . $portfolios_files[$files_numbers[$i]]);
                        }
						$portfolio = array();
						foreach ($html->find('div.thumb img') as $item) {
							$portfolio = array_merge($portfolio, array('thumb' => $item->src));
						}
						if (empty($portfolio['thumb'])) {
							if($files_numbers[$i]+1<count($portfolios_files)) {
								$test = false;
								for($j=$i;$j<6;$j++) {
									if($files_numbers[$j]+1<count($portfolios_files)) {
										$files_numbers[$j]++;
									} else {
										$files_numbers[$j] = $files_numbers[$j] - 1;
									}
								}
							} else {
								$files_numbers[$i]=-1;
								$test = true;
							}
						}
					} else {
						$files_numbers[$i]=-1;
					}
				}
			}while(!$test);
		}
		for ($i = 0; $i < 6; $i++) {
			if($files_numbers[$i]!=-1) {
                if($_POST["ua"]==="1") {
                    $html = file_get_html("https://pinguin-studio.com.ua/ua/" . $portfolios_files[$files_numbers[$i]]);
                } else {
                    $html = file_get_html("https://pinguin-studio.com.ua/" . $portfolios_files[$files_numbers[$i]]);
                }
				$portfolio = array();
				foreach ($html->find('div.thumb img') as $item) {
					$portfolio = array_merge($portfolio, array('thumb' => $item->src));
				}
				if (!empty($portfolio['thumb'])) {
					foreach ($html->find('h1.portfolio_inner_head') as $item) {
						$plaintext = $item->plaintext;
						$delimeter = mb_stripos($plaintext, " - ", 0, 'utf-8');
						$title = mb_substr($plaintext, 0, $delimeter, 'utf-8');
						$title = trim($title);
						$desc = mb_substr($plaintext, $delimeter+3, strlen($plaintext), 'utf-8');
						$desc = trim($desc);
						$portfolio = array_merge($portfolio, array('title' => iconv(mb_detect_encoding($title, "UTF-8,ISO-8859-1"), "UTF-8", $title)));
						$portfolio = array_merge($portfolio, array('desc' => iconv(mb_detect_encoding($desc, "UTF-8,ISO-8859-1"), "UTF-8", $desc)));
					}
					$portfolio = array_merge($portfolio,array('link'=>$portfolios_files[$files_numbers[$i]]));
					$portfolio = array_merge($portfolio,array('id'=>$files_numbers[$i]));
					array_push($portfolios, $portfolio);
				}
			}
		}
		/*foreach($portfolios as $portfolio) {
			echo var_dump($portfolio);
		}*/
		echo json_encode($portfolios);
	} else {
		echo "error";
	}
	exit;
