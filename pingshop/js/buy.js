$(document).ready(function(){
	$('a[href="#buy"]').click(function(e){
		e.preventDefault();
		$('#buy').css('pointer-events', 'auto');
		$('#buy').css('display', 'block');
		$('body').addClass('modal-open');
	});
	
	$('a[href="#buy1"]').click(function(e){
		e.preventDefault();
		$('#buy1').css('pointer-events', 'auto');
		$('#buy1').css('display', 'block');
		$('body').addClass('modal-open');
	});
	
	$('#buy a[href="#close"]').click(function(e){
		e.preventDefault();
		$('#buy').css('pointer-events', 'none');
		$('#buy').css('display', 'none');
		$('body').removeClass('modal-open');
		$('#buy .data').fadeIn();
		$('#buy .info').fadeOut();
        $('#buy .data .success_call_form').html('').fadeOut();
	});
	
	$('#buy1 a[href="#close"]').click(function(e){
		e.preventDefault();
		$('#buy1').css('pointer-events', 'none');
		$('#buy1').css('display', 'none');
		$('body').removeClass('modal-open');
        $('#buy1 .data').fadeIn();
        $('#buy1 .info').fadeOut();
        $('#buy1 .data .success_call_form').html('').fadeOut();
	});

	$('.modalDialog input[type="url"]').each(function() {
        $(this).mask("http://A", {
            translation: {
                "A": { pattern: /[\w\-.+]/, recursive: true }
            }
        });
    });

    $('.modalDialog input[type="email"]').each(function() {
        $(this).mask("A", {
            translation: {
                "A": { pattern: /[\w@\-.+]/, recursive: true }
            }
        });
    });
	
   //  var paypal = false;
   //
   // $('#buy form.data .submit-paypal').click(function(){
   //      paypal = true;
   // });
   //
   //  $('#buy form.data .submit-yandex').click(function(){
   //      paypal = false;
   //  });
   //
   // $('#buy form.data').submit(function(e){
   //      var furl = $('#buy form.data input[type="url"]').val();
   //      var femail = $('#buy form.data input[type="email"]').val();
   //      if(paypal) {
   //          $.ajax({
   //              type: "POST",
   //              url: "paypal.php",
   //              data: "domain=" + furl + "&email=" + femail,
   //              success: function (msg) {
   //                  $('#buy form.paypal .submit').click();
   //              }
   //          });
   //      } else {
   //          $.ajax({
   //              type: "POST",
   //              url: "yandex.php",
   //              data: "domain=" + furl + "&email=" + femail,
   //              success: function (msg) {
   //                  $('#buy form.yandex input[type="submit"]').click();
   //              }
   //          });
   //      }
   // });
    $('.data').each(function() {
        $(this).on('submit', function(e) {
            e.preventDefault();
            e.stopPropagation();

            var form = $(this);
            var furl = window.location.href;
            // var fname = $(this).find('input[name="txtname"]');
            // var fphone = $(this).find('input[name="txtphone"]');
            var fdomain = $(this).find('input[type="url"]');
            var femail = $(this).find('input[type="email"]');

             $.ajax({
                 type: "POST",
                 url: "forms.php",
                 data: "domain=" + $(fdomain).val() + "&email=" + $(femail).val() +'&form=1',
                 dataType: "text",
                 success: function (msg) {
                     console.log(msg);
                     switch(msg){
                         case 'success':
                             $(form).find('.success_call_form').fadeOut('fast', function() {
                                 $(form).find('.success_call_form').html('');
                                 $(form).find('.success_call_form').append('<div class="alert alert-success">Успешно отправлено</div>');
                                 $(form).find('.success_call_form').fadeIn();
                             });

                             $(fdomain).val('').attr('value', '');
                             $(femail).val('').attr('value', '');

                             setTimeout(function() {
                                 // window.location.hash = '#close';
                                 $(form).fadeOut('fast', function() {
                                     $(form).parent().find('.info').fadeIn('fast');
                                 });
                             }, 2000);

                             break;

                         case 'err':
                             $(form).find('.success_call_form').fadeOut('fast', function() {
                                 $(form).find('.success_call_form').html('');
                                 $(form).find('.success_call_form').append('<div class="alert alert-warning">Что-то пошло не так. Попробуйте позже</div>');
                                 $(form).find('.success_call_form').fadeIn();
                             })
                             break;

                         default:
                             break;
                     }
                 }
             });
        })
    });

    $('form.flex').each(function() {
        $(this).unbind('submit').on('submit', function(e) {
            e.preventDefault();
            e.stopPropagation();

            var form = $(this);
            var furl = window.location.href;
            var femail = $(this).find('input[name="email"]');

            $.ajax({
                type: "POST",
                url: "forms.php",
                data: "domain=" + furl + "&email=" + $(femail).val() +'&form=2',
                success: function (msg) {
                    switch(msg){
                        case 'err':
                            $(form).parent().parent().find('.resultPlace').fadeOut();
                            $(form).find('.error_wrap').fadeIn();
                            break;

                        case 'success':
                            $(form).find('.error_wrap').fadeOut();
                            $(femail).val('').attr('value', '');
                            $(form).parent().parent().find('.resultPlace').fadeIn();
                            break;

                        default:
                            break;
                    }
                }
            });
            return true;
        });
    });
});