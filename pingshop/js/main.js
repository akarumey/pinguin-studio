function counter(){var show=true;var countbox=".third_block_padding";$(window).on("scroll load resize",function(){if(!show)return false;var w_top=$(window).scrollTop();var e_top=$(countbox).offset().top;var w_height=$(window).height();var d_height=$(document).height();var e_height=$(countbox).outerHeight();if(w_top+200>=e_top||w_height+w_top==d_height||e_height+e_top<w_height){$(".spincrement").spincrement({from:0,thousandSeparator:"",duration:3000});show=false;}});}function preloader(){var hellopreloader=document.getElementById("hellopreloader_preload");function fadeOutnojquery(el){el.style.opacity=1;var interhellopreloader=setInterval(function(){el.style.opacity=el.style.opacity-0.05;if(el.style.opacity<=0.05){clearInterval(interhellopreloader);hellopreloader.style.display="none";}},16);new WOW().init();counter();}window.onload=function(){setTimeout(function(){fadeOutnojquery(hellopreloader);},1000);};}preloader();


$(document).ready(function(){
	//Плавный скролл
	$("#menu").on("click","a", function (event) {
		//отменяем стандартную обработку нажатия по ссылке
		event.preventDefault();

		//забираем идентификатор бока с атрибута href
		var id  = $(this).attr('href'),

		//узнаем высоту от начала страницы до блока на который ссылается якорь
			top = $(id).offset().top;
		
		//анимируем переход на расстояние - top за 1500 мс
		$('body,html').animate({scrollTop: top}, 1500);
	});

	//Параллакс
	if ($(window).width() > 1200) {
		$('#first_block').parallax();
		$('#design').parallax();
	}
	


	//Инициализация анимации
	new WOW().init();

	//Vue
	Vue.use(VeeValidate);

	// var form = new Vue({
	// 	el: '#newForm',
	// 	data:{
	// 		email:'',
	// 		show: false,
	// 	},
	// 	methods:{
	// 		validateBeforeSubmit() {
	// 			this.$validator.validateAll().then((result) => {
	// 				if (result) {
	// 				// eslint-disable-next-line
	// 					axios.post('/send_contacts.php', {
	// 						'email': this.email,
	// 					}).then((res)=>{
	// 						this.show=true;
	// 						this.email='';
	// 						Vue.nextTick(() => {
	// 							this.errors.clear()
	// 						})
	// 					}).catch((err)=>{
	// 						console.log(er)
	// 					});
	// 					return this.show=true;
	// 				}
	// 			});
	// 		}
	// 	}
	// });
	// var formSecong = new Vue({
	// 	el: '#newFormSecond',
	// 	data:{
	// 		email:'',
	// 		show: false,
	// 	},
	// 	methods:{
	// 		validateBeforeSubmit() {
	// 			this.$validator.validateAll().then((result) => {
	// 				if (result) {
	// 				// eslint-disable-next-line
	// 					axios.post('/send_contacts.php', {
	// 						'email': this.email,
	// 					}).then((res)=>{
	// 						this.show=true;
	// 						this.email='';
	// 						Vue.nextTick(() => {
	// 							this.errors.clear()
	// 						})
	// 					}).catch((err)=>{
	// 						console.log(er)
	// 					});
	// 					return this.show=true;
	// 				}
	// 			});
	// 		}
	// 	}
	// });

	$('.demomodal').each(function() {
		$(this).click(function(e){
			e.preventDefault();
			window.location.hash = '#demomodal';
		});
	})

	if ($(window).width() < 1200) {
		var nav = new Vue({
			el: '#nav_menu',
			data:{
				show: false,
				isActive: false
			},
			methods:{
				showNav(){
					if (this.show == false && this.isActive == false) {
						this.show = true;
						this.isActive = true; 
						return
					}else{
						this.show = false;
						this.isActive = false; 
					}
				}
			}
		});
	}


	//Слайдер в Умном функционале

	 $('.slider-for').slick({
	  slidesToShow: 1,
	  slidesToScroll: 1,
	  arrows: false,
	  fade: true,
	  asNavFor: '.slider-nav'
	});
	$('.slider-nav').slick({
	  slidesToShow: 1,
	  slidesToScroll: 1,
	  asNavFor: '.slider-for',
	  dots: true,
	  focusOnSelect: true
	});

	//Слайдер в Повышаем конверсию
	$('.slider-for_second').slick({
	  slidesToShow: 1,
	  slidesToScroll: 1,
	  arrows: false,
	  fade: true,
	  asNavFor: '.slider-nav_second'
	});
	$('.slider-nav_second').slick({
	  slidesToShow: 1,
	  slidesToScroll: 1,
	  asNavFor: '.slider-for_second',
	  dots: true,
	  focusOnSelect: true
	});
	
});

