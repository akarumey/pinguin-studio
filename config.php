<?php

define("PAYPAL_EMAIL","zakaz@pinguin-studio.com.ua");//почта адмнистритора

define("PAYPAL_MAIL_SUBJECT","Ping-Shop");//тема письма для отправки администратору
define("PAYPAL_MAIL_SUBJECT_CLIENT","Ping-Shop");//тема письма для отправки клиенту
define("PAYPAL_MAIL_CONTENT_CLIENT","<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01//EN\" \"http://www.w3.org/TR/1999/REC-html401-19991224/strict.dtd\">
<html><head>
		<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\">
		<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0\">
		<title>Сообщение от магазина {shop_name}</title>
		
		
		<style>	@media only screen and (max-width: 300px){ 
				body {
					width:218px !important;
					margin:auto !important;
				}
				thead, tbody{width: 100%}
				.table {width:195px !important;margin:auto !important;}
				.logo, .titleblock, .linkbelow, .box, .footer, .space_footer{width:auto !important;display: block !important;}		
				span.title{font-size:20px !important;line-height: 23px !important}
				span.subtitle{font-size: 14px !important;line-height: 18px !important;padding-top:10px !important;display:block !important;}		
				td.box p{font-size: 12px !important;font-weight: bold !important;}
				.table-recap table, .table-recap thead, .table-recap tbody, .table-recap th, .table-recap td, .table-recap tr { 
					display: block !important; 
				}
				.table-recap{width: 200px!important;}
				.table-recap tr td, .conf_body td{text-align:center !important;}	
				.address{display: block !important;margin-bottom: 10px !important;}
				.space_address{display: none !important;}	
			}
	@media only screen and (min-width: 301px) and (max-width: 500px) { 
				body {width:425px!important;margin:auto!important;}
				thead, tbody{width: 100%}
				.table {margin:auto!important;}	
				.logo, .titleblock, .linkbelow, .box, .footer, .space_footer{width:auto!important;display: block!important;}	
				.table-recap{width: 295px !important;}
				.table-recap tr td, .conf_body td{text-align:center !important;}
				.table-recap tr th{font-size: 10px !important}
				
			}
	@media only screen and (min-width: 501px) and (max-width: 768px) {
				body {width:478px!important;margin:auto!important;}
				thead, tbody{width: 100%}
				.table {margin:auto!important;}	
				.logo, .titleblock, .linkbelow, .box, .footer, .space_footer{width:auto!important;display: block!important;}			
			}
	@media only screen and (max-device-width: 480px) { 
				body {width:340px!important;margin:auto!important;}
				thead, tbody{width: 100%}
				.table {margin:auto!important;}	
				.logo, .titleblock, .linkbelow, .box, .footer, .space_footer{width:auto!important;display: block!important;}
				
				.table-recap{width: 295px!important;}
				.table-recap tr td, .conf_body td{text-align:center!important;}	
				.address{display: block !important;margin-bottom: 10px !important;}
				.space_address{display: none !important;}	
			}
</style>

	</head>
	<body style=\"-webkit-text-size-adjust:none;background-color:#fff;width:650px;font-family:Open-sans, sans-serif;color:#555454;font-size:13px;line-height:18px;margin:auto\">
<table class=\"table table-mail\" style=\"width: 100%; margin-top: 10px; -moz-box-shadow: 0 0 5px #afafaf; -webkit-box-shadow: 0 0 5px #afafaf; -o-box-shadow: 0 0 5px #afafaf; box-shadow: 0 0 5px #afafaf; filter: progid:DXImageTransform.Microsoft.Shadow(color=#afafaf,Direction=134,Strength=5);\">
<tbody>
<tr>
<td class=\"space\" style=\"width: 20px; padding: 7px 0;\">&nbsp;</td>
<td align=\"center\" style=\"padding: 7px 0;\">
<table class=\"table\" bgcolor=\"#ffffff\" style=\"width: 100%;\">
<tbody>
<tr>
<td align=\"center\" class=\"logo\" style=\"border-bottom: 4px solid #333333; padding: 7px 0; background: #fff;\"><a title=\"PingShop\" href=\"https://hotshop.pinguin-studio.com.ua/\" style=\"color: #337ff1;\"> <img src=\"https://pinguin-studio.com.ua/pinguin-shop-logo.png\" alt=\"Hot Shop\"> </a></td>
</tr>
<tr>
<td align=\"center\" class=\"titleblock\" style=\"padding: 7px 0; \"><span size=\"2\" face=\"Open-sans, sans-serif\" color=\"#555454\" style=\"color: #555454; font-family: Open-sans, sans-serif; font-size: small;\"> <span class=\"title\" style=\"font-weight: 500; font-size: 28px; text-transform: uppercase; line-height: 33px;\">Здравствуйте</span><br> <span class=\"subtitle\" style=\"font-weight: 500; font-size: 16px; text-transform: uppercase; line-height: 25px;\">Благодарим за внимание к нашему продукту!</span> </span></td>
</tr>







<tr>
<td class=\"linkbelow\" style=\"padding: 7px 0;\"><span size=\"2\" face=\"Open-sans, sans-serif\" color=\"#555454\" style=\"color: #555454; font-family: Open-sans, sans-serif; font-size: small;\"> <span> Вы выбрали наш продукт - <strong>интернет-магазин <a href=\"https://hotshop.pinguin-studio.com.ua/\" style=\"color: #337ff1;\">PINGSHOP</a></strong>. <p>Сейчас происходит формирование архива для указанного Вами домена. Архив сайта с инструкцией по установке будет выслан Вам на почту</strong>. На формирование архива необходимо <strong>2 часа</strong>. Поэтому, если вы сделали заказ вечером, то архив получите на следующий рабочий день.</p> <p>Если у Вас возникают проблемы с установкой сайта на Ваш хостинг, или Вы просто не знаете как это делать, то мы поможем Вам в этом абсолютно <strong>БЕСПЛАТНО</strong>! Для этого ответным письмом пришлите следующие данные: <br><strong>Домен</strong> (на котрой делали заказ), <br><strong>FTP доступ</strong> (хост, логин и пароль) <br><strong>Mysql доступ</strong> (хост, база, логин и пароль) <br>или доступ в панель управления хостингом.</p> <p>Если Вам необходима помощь в выборе хостинга, напишите нам и мы предложим Вам варианты для Вашего сайта.</p> <p>Требования к хостингу для сайта ниже в этом письме.</p></span> </span>
<p></p>
<p style=\"
    text-align: center;
\"><strong>ВАЖНО!!!</strong> <br><br>
<strong>После установки магазин работает 5 дней</strong><br><br> За это время Вы должны ознакомиться с ним и оплатить его бессрочную лицензию для домена.<br>
Интернет-магазин формируется именно под Ваш домен. <br>Установка его на другой домен будет <strong>невозможна</strong>. <br>Тем не менее, все исходные файлы доступны для редактирования любым разработчиком. <br>В случае, если Вам необходима смена домена на новый, Вы можете написать запрос нам на почту, в чат поддержки на сайте или в скайп. Мы сформируем новые файлы для Вас бесплатно.</p>
<p></p>
<p></p>
<p><strong>Технические требования к хостингу</strong> <br>
Рекомендованный веб-сервер: <strong>Apache 2.x или Nginx</strong><br>
PHP: <strong>5.4+</strong><br>
MySQL: <strong>5.0+</strong><br>
FTP-доступ<br>
memory_limit: <strong>128M</strong><br> 
upload_max_filesize: <strong>16M (или больше, если доступно)</strong><br>
Расширения PHP: <strong>Mcrypt, OpenSSL, Zip, Curl, GD, PDO</strong><br>
Для улучшения производительности: <strong>расширение MemCached, mcrypt PHP</strong></p>    
</td>
</tr>


<tr>
<td class=\"footer\" style=\"border-top: 4px solid #22c1c1;padding: 7px 0;\"><p><strong>Контакты для связи:</strong></p>

<p>Электронная почта: <a href=\"mailto:zakaz@pinguin-studio.com.ua\" class=\"mail_a\">zakaz@pinguin-studio.com.ua</a></p>
<p>Skype: <a href=\"skype:akvadon?chat\">Связаться с нами</a></p>
<p>Телефон: (068) 728-89-89, (095) 339-73-79</p>

</td>
</tr>
</tbody>
</table>
</td>
<td class=\"space\" style=\"width: 20px; padding: 7px 0;\">&nbsp;</td>
</tr>
</tbody>
</table>

</body></html>");//текст письма для отправки клиенту

define("PAYPAL_BUTTON","Оплатить Paypal");//текст кнопки оплаты Paypal

define("PAYPAL_ENCRYPTED","-----BEGIN PKCS7-----MIIHTwYJKoZIhvcNAQcEoIIHQDCCBzwCAQExggEwMIIBLAIBADCBlDCBjjELMAkGA1UEBhMCVVMxCzAJBgNVBAgTAkNBMRYwFAYDVQQHEw1Nb3VudGFpbiBWaWV3MRQwEgYDVQQKEwtQYXlQYWwgSW5jLjETMBEGA1UECxQKbGl2ZV9jZXJ0czERMA8GA1UEAxQIbGl2ZV9hcGkxHDAaBgkqhkiG9w0BCQEWDXJlQHBheXBhbC5jb20CAQAwDQYJKoZIhvcNAQEBBQAEgYBnw7+9vzsWX14GpXBbg8DAQP73nv43PR6mD6kh6vVqWA09HoPHGPl4n9FhpuF241be0D1qqn8R2DNWp5Sgt5TBMG4AZ9Zj33wy057B+d0COlOwT3HHkEQ1HHkU29jPDgbWcayvfAqBgsFoGY23oEKYzvmUVdk42pqJMGxcGKDUmjELMAkGBSsOAwIaBQAwgcwGCSqGSIb3DQEHATAUBggqhkiG9w0DBwQIh00AdgCLjIyAgaj1nrumaM6YFMZdBaQsRWrqUs+WYw7bbj3tZNhvMckwp8zpUufT6sMlyRoPOA7hP+QKzstyAH0P1huGsNfxSJaCrjLoDqBboBeWbjf82VDLEzK8ldgn0f6J1xvCR9+eN6jLCNq710D0GXf+unTRx5rTYKTmvOjgjInX/rK3F1swA+wHTky/ClSjUTVwrFPRJtHtAbVutZ7Xkrs5xfpshY9rBxFjt2b8/9KgggOHMIIDgzCCAuygAwIBAgIBADANBgkqhkiG9w0BAQUFADCBjjELMAkGA1UEBhMCVVMxCzAJBgNVBAgTAkNBMRYwFAYDVQQHEw1Nb3VudGFpbiBWaWV3MRQwEgYDVQQKEwtQYXlQYWwgSW5jLjETMBEGA1UECxQKbGl2ZV9jZXJ0czERMA8GA1UEAxQIbGl2ZV9hcGkxHDAaBgkqhkiG9w0BCQEWDXJlQHBheXBhbC5jb20wHhcNMDQwMjEzMTAxMzE1WhcNMzUwMjEzMTAxMzE1WjCBjjELMAkGA1UEBhMCVVMxCzAJBgNVBAgTAkNBMRYwFAYDVQQHEw1Nb3VudGFpbiBWaWV3MRQwEgYDVQQKEwtQYXlQYWwgSW5jLjETMBEGA1UECxQKbGl2ZV9jZXJ0czERMA8GA1UEAxQIbGl2ZV9hcGkxHDAaBgkqhkiG9w0BCQEWDXJlQHBheXBhbC5jb20wgZ8wDQYJKoZIhvcNAQEBBQADgY0AMIGJAoGBAMFHTt38RMxLXJyO2SmS+Ndl72T7oKJ4u4uw+6awntALWh03PewmIJuzbALScsTS4sZoS1fKciBGoh11gIfHzylvkdNe/hJl66/RGqrj5rFb08sAABNTzDTiqqNpJeBsYs/c2aiGozptX2RlnBktH+SUNpAajW724Nv2Wvhif6sFAgMBAAGjge4wgeswHQYDVR0OBBYEFJaffLvGbxe9WT9S1wob7BDWZJRrMIG7BgNVHSMEgbMwgbCAFJaffLvGbxe9WT9S1wob7BDWZJRroYGUpIGRMIGOMQswCQYDVQQGEwJVUzELMAkGA1UECBMCQ0ExFjAUBgNVBAcTDU1vdW50YWluIFZpZXcxFDASBgNVBAoTC1BheVBhbCBJbmMuMRMwEQYDVQQLFApsaXZlX2NlcnRzMREwDwYDVQQDFAhsaXZlX2FwaTEcMBoGCSqGSIb3DQEJARYNcmVAcGF5cGFsLmNvbYIBADAMBgNVHRMEBTADAQH/MA0GCSqGSIb3DQEBBQUAA4GBAIFfOlaagFrl71+jq6OKidbWFSE+Q4FqROvdgIONth+8kSK//Y/4ihuE4Ymvzn5ceE3S/iBSQQMjyvb+s2TWbQYDwcp129OPIbD9epdr4tJOUNiSojw7BHwYRiPh58S1xGlFgHFXwrEBb3dgNbMUa+u4qectsMAXpVHnD9wIyfmHMYIBmjCCAZYCAQEwgZQwgY4xCzAJBgNVBAYTAlVTMQswCQYDVQQIEwJDQTEWMBQGA1UEBxMNTW91bnRhaW4gVmlldzEUMBIGA1UEChMLUGF5UGFsIEluYy4xEzARBgNVBAsUCmxpdmVfY2VydHMxETAPBgNVBAMUCGxpdmVfYXBpMRwwGgYJKoZIhvcNAQkBFg1yZUBwYXlwYWwuY29tAgEAMAkGBSsOAwIaBQCgXTAYBgkqhkiG9w0BCQMxCwYJKoZIhvcNAQcBMBwGCSqGSIb3DQEJBTEPFw0xODA0MTgxMTI3NTFaMCMGCSqGSIb3DQEJBDEWBBQhX2XJnLyPu1l2mjSB3VTr0eGAETANBgkqhkiG9w0BAQEFAASBgE+bocpXY5zOL2Y3iXJQzPSrYIpCd7sKQBwMytC3NOeIGenRzqzDvo3XZajTsRe+O2luA4OmpXKs6X/MtMEsPwBFT70i0eYQm87rSx5C87+dS5+cObLaux142FZPx2rkKpX+t3U0PFZ33TFP1lhAPOgpjkE/qlhuBcH33Qlcmfhk-----END PKCS7-----");//paypal

define("YANDEX_EMAIL",PAYPAL_EMAIL);//почта адмнистритора

define("YANDEX_MAIL_SUBJECT",PAYPAL_MAIL_SUBJECT);//тема письма для отправки администратору
define("YANDEX_MAIL_SUBJECT_CLIENT",PAYPAL_MAIL_SUBJECT_CLIENT);//тема письма для отправки клиенту
define("YANDEX_MAIL_CONTENT_CLIENT",PAYPAL_MAIL_CONTENT_CLIENT);//текст письма для отправки клиенту

define("YANDEX_BUTTON","Оплатить Yandex");//текст кнопки оплаты Yandex

define("YANDEX_TRANSACTION_NAME","Покупка магазина PingShop");//Название проекта для перевода
define("YANDEX_TRANSACTION_APPOINTMENT","Покупка магазина PingShop");//назначение перевода
define("YANDEX_TRANSACTION_SUM","100000");//сумма перевода
define("YANDEX_TRANSACTION_RECEIVER","41001943592305");//яндекс кошелек

define('ZAKAZ_MAIL', 'zakaz@pinguin-studio.com.ua');
define('ZAKAZ_PHONE_SUBJECT', 'Форма заказа с пингшопа Украина');
define('ZAKAZ_EMAIL_SUBJECT', 'Форма мейла с пингшопа Украина');

define('FAST_DOWNLOAD', 'Скачать бесплатно');

define('SMTP_SERVER','mail.adm.tools');
define('SMTP_PORT','25');
define('SMTP_USER','admin@pinguin-studio.com.ua');
define('SMTP_PASS','N7zr66Nk');
define('SMTP_NAME','PingShop');