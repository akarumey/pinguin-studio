<?php

include "config.php";

include "./phpmailer/Exception.php";
include "./phpmailer/SMTP.php";
include "./phpmailer/PHPMailer.php";

use PHPMailer\PHPMailer\PHPMailer;

if ($_SERVER['REQUEST_METHOD'] != "POST") {
    header("Location: /");
    exit;
}

if(isset($_POST["domain"]) && !empty($_POST["domain"]) && isset($_POST['form']) && !empty($_POST['form'])) {
    switch($_POST['form'])
    {
        case 1:
            if(isset($_POST["domain"]) && !empty($_POST["domain"]) && isset($_POST["email"]) && !empty($_POST["email"])) {
                mail(ZAKAZ_MAIL, ZAKAZ_PHONE_SUBJECT, "Domain: ".$_POST["domain"]."\nE-mail: ".$_POST["email"]);
                date_default_timezone_set('Etc/UTC');
                $mail = new PHPMailer;
                $mail->isSMTP();
                $mail->SMTPDebug = 0;
                $mail->Host = SMTP_SERVER;
                $mail->Port = SMTP_PORT;
                $mail->SMTPAuth = true;
                $mail->Username = SMTP_USER;
                $mail->Password = SMTP_PASS;
                $mail->setFrom(SMTP_USER, SMTP_NAME);
                $mail->addAddress($_POST["email"], '');
                $mail->Subject = PAYPAL_MAIL_SUBJECT_CLIENT;
                $mail->Body = PAYPAL_MAIL_CONTENT_CLIENT;
                $mail->IsHTML(true);
                if($mail->send())
                    echo 'success';
                else
                    echo 'err';
            } else {
                echo 'err';
            }
                exit;
            break;
        case 2:
            if(isset($_POST["email"]) && !empty($_POST["email"]))
            {
                mail(ZAKAZ_MAIL, ZAKAZ_EMAIL_SUBJECT, "Domain: ".$_POST["domain"]."\nE-mail: ".$_POST["email"]);
                echo 'success';
            } else {
                echo 'err';
            }
            exit;
            break;
        default:
            echo 'err';
            exit;
            break;
    }
} else {
    echo 'err';
    exit;
}