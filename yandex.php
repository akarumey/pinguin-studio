<?php
include "config.php";

if ($_SERVER['REQUEST_METHOD'] != "POST") {
    header("Location: /");
    exit;
}

if(isset($_POST["domain"]) && !empty($_POST["domain"]) && isset($_POST["email"]) && !empty($_POST["email"])) {
    mail(YANDEX_EMAIL, YANDEX_MAIL_SUBJECT, "Domain: ".$_POST["domain"]."\nEmail: ".$_POST["email"]);
    mail(($_POST["email"]),YANDEX_MAIL_SUBJECT_CLIENT, YANDEX_MAIL_CONTENT_CLIENT);
    echo "ok";
    exit;
} else {
    header("Location: /");
    exit;
}