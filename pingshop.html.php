<?php include "config.php";
require_once "mobile-detect/Mobile_Detect.php";
$detect = new Mobile_Detect; ?>
<!DOCTYPE html>
<html lang="ru">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Интернет-магазин "Быстрый старт" от Pinguin-Studio за 9900 грн. и за 3 дня</title>
	<meta name="description" content="Заказать создание, разработку интернет магазина недорого, Быстрый Старт - цена Вас заинтересует 9900 грн, узнать стоимость ☎ 068 728 89 89 ">
	<link rel="stylesheet" href="pingshop/css/normalize.css">
	<link rel="stylesheet" href="pingshop/css/font-awesome.min.css">
	<link rel="stylesheet" href="pingshop/css/flexboxgrid.css">
	<link rel="stylesheet" href="pingshop/css/slick.css">
	<link rel="stylesheet" href="pingshop/css/animate.css">
	<link rel="stylesheet" href="pingshop/css/main.css">

	<link href="https://fonts.googleapis.com/css?family=Playfair+Display:900i&amp;subset=cyrillic" rel="stylesheet">
    <style>
        body.modal-open {
            overflow: hidden;
        <?php
        if(!$detect->isMobile() && !$detect->isTablet()) {
            ?>
            margin-right: 17px;
        <?php
    }
    ?>
        }
    </style>
		<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-117832829-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-117832829-1');
</script>
<!-- Google Analytics -->
<script>
	(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

	ga('create', 'UA-117832829-1', 'auto');
	ga('send', 'pageview');
</script>

<!-- Yandex.Metrika counter -->
<script type="text/javascript" >
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter49374475 = new Ya.Metrika2({
                    id:49374475,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true,
                    webvisor:true
                });
            } catch(e) { }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://d31j93rd8oukbv.cloudfront.net/metrika/tag.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks2");
</script>
<noscript><div><img src="https://d31j93rd8oukbv.cloudfront.net/watch/49374475" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->

<!-- End Google Analytics -->
</head>
<body id="index">
    <div id="hellopreloader"><div id="hellopreloader_preload"></div></div>
	<div class="first_block_wrapper">
		<div class="first_block animated-background" id="first_block">
			<header>
				<div class="container-fluid header_border">
					<div class="row flex_wrap align_center first_block_inner_nav">
						<div class="col-xs-7 col-sm-4 col-lg-3 ">
							<a href="/">
								<img src="pingshop/img/logo.png" alt="" class="img-responsive">
							</a>
						</div>
						<div class="col-xs-4 col-sm-3 col-lg-6 no_padding_right navigation">
							<nav id="nav_menu">  
								<div class="no_margin" v-on:click="showNav">
									<p class="tablet_menu_text">Меню</p>
									<button class="ham close_menu" v-bind:class="{ opened: isActive }">
										<span class="bar"></span>
										<span class="bar"></span>
										<span class="bar"></span>
									</button>
								</div>
								<transition name="fade">
									<ul class="flex flex_wrap nav_list" id="menu" v-if="show">
										<li><a href="#obzor">Обзор</a></li>
										<li><a href="#design">Дизайн</a></li>
										<li><a href="#functional">Функционал</a></li>
										<li><a href="#demomodal" class="demomodal" data-modal="#demomodal">Демо</a></li>
										<li class="nav_list_last_item"><a href="#buy" class="call_back">Скачать бесплатно</a></li>
									</ul>
								</transition>    
							</nav>
						</div>
						<div class="col-sm-3 flex justify_content_end call_back_wrap">
							<a href="#buy" class="call_back">Скачать бесплатно</a>
						</div>
					</div>
				</div>
			</header>
			<div class="container-fluid first_block_inner">
				<div class="row">
					<div class="col-xs-12 col-lg-7">
						<div class="row no_margin">
							<div class="col-xs-12"></div>
							<div class="first_head wow slideInUp">
								<p class="first_bold_text">БЫСТРЫЙ СТАРТ!</p>
								<p class="first_italic_text">Интернет-магазин за 3 дня!</p>
								<p class="first_italic_text">Всего за 9900 грн.!</p>
							</div>
							<p class="first_text_plus wow slideInUp">
								Плюс хостинг и домен на 1 год в подарок!
							</p>
						</div>
						<div class="row no_margin text_center_mobile">
							<div class="col-xs-12 col-sm-4 col-lg-12">
                                <a href="#buy" class="call_back big bubble left wow slideInUp">Скачать бесплатно</a>
								<a href="https://hotshop.pinguin-studio.com.ua/" target="_blank" class="demomodal call_back transparent bubble left wow slideInUp">Cмотреть демо</a>
							</div>
							<div class="col-xs-12 col-sm-8">
								<div id="shop_effects_mobile">
									<img src="pingshop/img/new_img/devices_tablet.png" alt="" class="img-responsive mac_img">
								</div>
							</div>
						</div>

									 
					</div>
					<div id="shop_effects" class="shop_effect">
						<div class="shop_effect_one shop_effect layer" data-depth="0.2">
							<img src="pingshop/img/new_img/phone_air.png" alt="" class="img-responsive mac_img">
						</div>
						<div class="shop_effect_two shop_effect layer" data-depth="0.4">
							<img src="pingshop/img/new_img/ipad_air.png" alt="" class="img-responsive mac_img">
						</div>
						<div class=" shop_effect_three shop_effect layer" data-depth="0.6">
							<img src="pingshop/img/new_img/mac_air.png" alt="" class="img-responsive mac_img">
						</div>
					</div>
				</div>
			</div>
		</div>
		<a href="#second_block" class="see_more"><img src="pingshop/img/new_img/discuss_icon.png" alt=""></a>
	</div>
    <div id="buy" class="modalDialog">
        <div>
            <a href="#close" title="Закрыть" class="close">X</a>
            <!--										<form class="data">-->
            <!--											<input type="url" placeholder="Домен" required>-->
            <!--											<input type="email" placeholder="Email" required>-->
            <!--											<input class="submit-paypal call_back big bubble left wow" type="submit" value="--><?php //echo PAYPAL_BUTTON; ?><!--">-->
            <!--                                            <input class="submit-yandex call_back big bubble left wow" type="submit" value="--><?php //echo YANDEX_BUTTON; ?><!--">-->
            <!--										</form>-->
            <!--										<form class="paypal hidden" action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top" style="display: inline-block">-->
            <!--											<input type="hidden" name="cmd" value="_s-xclick">-->
            <!--											<input type="hidden" name="encrypted" value="--><?php //echo PAYPAL_ENCRYPTED; ?><!--">-->
            <!--											<input type="image" class="submit call_back big bubble left wow" border="0" name="submit" alt="Оплатить" value="Оплатить">-->
            <!--											<img alt="" border="0" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" width="1" height="1">-->
            <!--										</form>-->
            <!--                                        <form class="yandex hidden" method="POST" action="https://money.yandex.ru/quickpay/confirm.xml">-->
            <!--                                            <input type="hidden" name="receiver" value="--><?php //echo YANDEX_TRANSACTION_RECEIVER; ?><!--">-->
            <!--                                            <input type="hidden" name="formcomment" value="--><?php //echo YANDEX_TRANSACTION_NAME; ?><!--">-->
            <!--                                            <input type="hidden" name="short-dest" value="--><?php //echo YANDEX_TRANSACTION_APPOINTMENT; ?><!--">-->
            <!--                                            <input type="hidden" name="targets" value="--><?php //echo YANDEX_TRANSACTION_APPOINTMENT; ?><!--">-->
            <!--                                            <input type="hidden" name="quickpay-form" value="donate">-->
            <!--                                            <input type="hidden" name="sum" value="--><?php //echo YANDEX_TRANSACTION_SUM; ?><!--" data-type="number">-->
            <!--                                            <input type="hidden" name="need-fio" value="true">-->
            <!--                                            <input type="hidden" name="need-email" value="true">-->
            <!--                                            <input type="hidden" name="need-phone" value="true">-->
            <!--                                            <input type="hidden" name="need-address" value="false">-->
            <!--                                            <input type="submit" value="Перевести">-->
            <!--                                        </form>-->
            <!--                                        <img alt="" class="modal_logo" data-pagespeed-url-hash="4123841404" src="img/modal_2_top.png" onerror="this.onerror=null;pagespeed.lazyLoadImages.loadIfVisibleAndMaybeBeacon(this);">-->
            <!--                                        <h2 class="modal_head">Закажите обратный звонок</h2>-->
            <!--                                        <p class="modal_text">и мы свяжемся с Вами в ближайшее время</p>-->
            <!--                                        <form class="fofmCall" action="" enctype="multipart/form-data">-->
            <!--                                            <div class="modal-content_items">-->
            <!--                                                <input type="text" required="" placeholder="Имя" name="txtname" class="modal_border">-->
            <!--                                                <input type="tel" required="" placeholder="Телефон" name="txtphone" class="modal_border" id="modal_tel_call">-->
            <!--                                            </div>-->
            <!--                                            <span class="discuss_us_modal discuss_us flex align_center"><input type="submit" class="discuss_us_input" value="Заказать звонок" name="btnsend"></span>-->
            <!---->
            <!--                                            <p class="success_call_form"></p>-->
            <!--                                        </form>-->
            <!--<img alt="" class="modal_logo" data-pagespeed-url-hash="4123841404" src="img/modal_2_top.png" onerror="this.onerror=null;pagespeed.lazyLoadImages.loadIfVisibleAndMaybeBeacon(this);">
            <h2 class="modal_head">Закажите обратный звонок</h2>
            <p class="modal_text">и мы свяжемся с Вами в ближайшее время</p>
            <form class="fofmCall" action="" enctype="multipart/form-data">
                <div class="modal-content_items">
                    <input type="text" required="" placeholder="Имя" name="txtname" class="modal_border">
                    <input type="tel" required="" placeholder="Телефон" name="txtphone" class="modal_border" id="modal_tel_call">
                </div>
                <span class="discuss_us_modal discuss_us flex align_center"><input type="submit" class="discuss_us_input" value="Заказать звонок" name="btnsend"></span>

                <div class="success_call_form"></div>
            </form>-->
            <form class="data">
                <div>
                    <p class="alert alert-info">
                        Для получения файлов интернет-магазина заполните форму. <br>Мы сформируем архив для Вашего домена и отправим Вам со всеми неоходимыми инструкциями. <br>Установка бесплатная!
                    </p>
                </div>
                <input type="url" placeholder="Домен (в виде http://test.com)" required>
                <input type="email" placeholder="Email" required>
                <input class="submit-yandex call_back big bubble left wow" type="submit" style="margin:0 auto;visibility: visible;" value="<?php echo FAST_DOWNLOAD; ?>">

                <div class="success_call_form"></div>
            </form>
            <div class="info" style="display:none">
                <div class="alert alert-info">
                    Спасибо! Архив уже формируется ожидайте его на своей почте в ближайшее время!
                </div>
            </div>
        </div>
    </div>
	<div class="second_block" id="second_block">
		<div class="container-fluid">
			<div class="row">
				<div class="col-xs-12">
					<div class="profi_wrap wow slideInUp">
						<p class="profi">Это предложение <span class="profi_bold">для Вас если:</span></p>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12 col-sm-6 col-lg-5 second_block_item wow slideInUp">
					<div class="row no_margin">
						<div class="col-xs-12 col-sm-3 col-md-3 no_padding second_block_inner_img">
							<img src="pingshop/img/new_img/second_block_1.png" alt="" class="img">
						</div>
						<div class="col-xs-12 col-sm-9 col-md-9 no_padding_right">
							<h3 class="second_block_headings">Вы открываете свой первый  Интернет-магазин?</h3>
							<p class="second_block_text">Не наступайте на грабли.  Используйте проверенное решение.</p>
						</div>
					</div>
				</div>
				<div class="col-xs-12 col-sm-6 col-lg-5 col-lg-offset-1 second_block_item wow slideInUp">
					<div class="row no_margin">
						<div class="col-xs-12 col-sm-3 col-md-3 no_padding second_block_inner_img">
							<img src="pingshop/img/new_img/second_block_2.png" alt="" class="img">
						</div>
						<div class="col-xs-12 col-sm-9 col-md-9 no_padding_right">
							<h3 class="second_block_headings">Вы хотите обновить свой  Интернет-магазин?</h3>
							<p class="second_block_text">Мы поможем перенести товары  в новый магазин. (при наличии подготовленного XLS или CSV файла)</p>
						</div>
					</div>
				</div>
				<div class="col-xs-12 col-sm-5 second_block_item last wow slideInUp">
					<div class="row no_margin">
						<div class="col-xs-12 col-sm-3 col-md-3 no_padding second_block_inner_img">
							<img src="pingshop/img/new_img/second_block_3.png" alt="" class="img">
						</div>
						<div class="col-xs-12 col-sm-9 col-md-9 no_padding_right">
							<h3 class="second_block_headings">Продаете на торговых  площадках Prom, zakupka?</h3>
							<p class="second_block_text">Откройте собственный магазин  и управляйте прибылью по своему желанию!</p>
						</div>
					</div>
				</div>
				<div class="col-xs-12 col-sm-5 col-sm-offset-1 second_block_item last wow slideInUp">
					<div class="row no_margin">
						<div class="col-xs-12 col-sm-3 col-md-3 no_padding second_block_inner_img">
							<img src="pingshop/img/new_img/second_block_4.png" alt="" class="img">
						</div>
						<div class="col-xs-12 col-sm-9 col-md-9 no_padding_right">
							<h3 class="second_block_headings">У Вас есть розничная точка  или магазин?</h3>
							<p class="second_block_text">Привлекайте массово клиентов  из интернет!</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="third_block">
		<div class="container-fluid">
			<div class="row">
				<div class="col-xs-12 wow slideInUp">
					<div class="profi_wrap">
						<p class="profi">За 3 дня Вы получите</p>
						<p class="profi_bold">Интернет-магазин, который продает:</p>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12 col-sm-4 third_block_item first wow slideInUp">
					<div class="row">
						<div class="col-xs-3 col-sm-3 text_center">
							<span class="play_fair">1</span>
						</div>
						<div class="col-xs-9 col-sm-9">
							<h3 class="second_block_headings">Адаптивный <br> дизайн </h3>
							<p class="second_block_text">для мобильных устройств</p>
						</div>
					</div>
				</div>
				<div class="col-xs-12 col-sm-4 third_block_item second wow slideInUp">
					<div class="row">
						<div class="col-xs-3 col-sm-3 text_center">
							<span class="play_fair">2</span>
						</div>
						<div class="col-xs-9 col-sm-9">
							<h3 class="second_block_headings">Мощный <br> функционал </h3>
							<p class="second_block_text">для увеличения продаж</p>
						</div>
					</div>
				</div>
				<div class="col-xs-12 col-sm-4 third_block_item third wow slideInUp">
					<div class="row">
						<div class="col-xs-3 col-sm-3 text_center">
							<span class="play_fair">3</span>
						</div>
						<div class="col-xs-9 col-sm-9">
							<h3 class="second_block_headings">Удобное <br> наполнение </h3>
							<p class="second_block_text">и выгрузка товаров</p>
						</div>
					</div>
				</div>
				<div class="col-xs-12 col-sm-4 third_block_item fourth wow slideInUp">
					<div class="row">
						<div class="col-xs-3 col-sm-3 text_center">
							<span class="play_fair">4</span>
						</div>
						<div class="col-xs-9 col-sm-9">
							<h3 class="second_block_headings">Подготовлен <br> к продвижению </h3>
							<p class="second_block_text">в поисковиках (Google, Yandex)</p>
						</div>
					</div>
				</div>
				<div class="col-xs-12 col-sm-4 third_block_item fifth wow slideInUp">
					<div class="row">
						<div class="col-xs-3 col-sm-3 text_center">
							<span class="play_fair">5</span>
						</div>
						<div class="col-xs-9 col-sm-9">
							<h3 class="second_block_headings">Простая <br> оплата  товара </h3>
							<p class="second_block_text">Приват24, PayPal и другие</p>
						</div>
					</div>
				</div>
				<div class="col-xs-12 col-sm-4 third_block_item sixth wow slideInUp">
					<div class="row">
						<div class="col-xs-3 col-sm-3 text_center">
							<span class="play_fair">6</span>
						</div>
						<div class="col-xs-9 col-sm-9">
							<h3 class="second_block_headings">Размещение <br> в Интернете </h3>
							<p class="second_block_text">на скоростном <br>  SSD-хостинге</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="fourth_block">
		<div class="container-fluid">
			<div class="row align_center">
				<div class="col-xs-12 col-lg-6 fourth_block_item">
					<h3 class="fourth_block_head wow slideInUp">Хотите увеличить средний чек и прибыль в несколько раз? </h3>
					<h4 class="fourth_block_head_bold wow slideInUp">Отправьте заявку!</h4>
				</div>
				<div class="col-xs-12 col-lg-6 fourth_block_item">
					
					<div class="columns is-multiline" id="newForm">
						<div class="newForm_inner">
							<form action="POST" @submit.prevent='validateBeforeSubmit' class="flex">
								<div class="column is-12">
									<p class="control has-icon has-icon-right">
											<input name="email" v-model="email" v-validate.initial="'required|email'" :class="{'input': true, 'is-danger': errors.has('email') }" type="email" placeholder="Ваш Email..." autocomplete="off">
											<span class="error_wrap" style="display: none;">
												<i v-show="errors.has('email')" class="fa fa-warning"></i>
												<span v-show="errors.has('email')" class="help is-danger">
													Обязательное поле.
												</span>
											</span>
									</p>
								</div>
								<button>Отправить</button>
							</form>
						</div>
						<div class="resultPlace" style="display: none;" v-if="show">
							<p class="result">Спасибо, данные отправлены! <i class="fa fa-check" aria-hidden="true"></i></p>	
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="fifth_block text-center" id="design">
		<div id="shop_effects_fourth" class="shop_effect_fourth">
			<div class="shop_effect_one_fifth shop_effect layer" data-depth="0.2">
				<img src="pingshop/img/new_img/fifth_block_imac.png" alt="" class="img-responsive mac_img">
			</div>
			<div class="shop_effect_two_fifth shop_effect layer" data-depth="0.3">
				<img src="pingshop/img/new_img/fifth_block_logo.png" alt="" class="img-responsive mac_img">
			</div>
			<div class=" shop_effect_three_fifth shop_effect layer" data-depth="0.4">
				<img src="pingshop/img/new_img/fifth_block_menu.png" alt="" class="img-responsive mac_img">
			</div>
			<div class="shop_effect_four_fifth shop_effect layer" data-depth="0.5">
				<img src="pingshop/img/new_img/fifth_block_screen.png" alt="" class="img-responsive mac_img">
			</div>
			<div class=" shop_effect_five_fifth shop_effect layer" data-depth="0.6">
				<img src="pingshop/img/new_img/fifth_block_iPhone.png" alt="" class="img-responsive mac_img">
			</div>
		</div>
		<div class="container-fluid">
			<div class="row">
				<div class="col-xs-12 col-sm-5 col-sm-offset-7 col-lg-7 col-lg-offset-5 fifth_block_colums">
					<h3 class="some_about_us wow slideInUp">
						АДАПТИВНЫЙ ДИЗАЙН
					</h3>
					<p class="some_about_us_text wow slideInUp">
						До 60% посетителей заходят в Интернет-магазин через мобильные устройства! Если у Вашего сайта нет мобильной версии, Вы теряете клиентов, продажи, прибыль.
					</p>
					<ul class="fifth_block_list wow slideInUp">
						<li>
							<p>- 4 разрешения экрана: 
							<span>1920, 1280, 750, 320 px</span>
							</p>
						</li>
						<li><p>- "движок" - популярная система управления PrestaShop</p></li>
						<li><p>- 15 сверстанных страниц</p></li>
						<li><p>- самостоятельный выбор цветовой гаммы</p></li>
					</ul>
					<a href="https://hotshop.pinguin-studio.com.ua/" target="_blank" class="demomodal call_back big wow slideInUp">
						Смотреть демо
					</a>
				</div>

			</div>
		</div>
	</div>
	<div class="sixth_block six" id="functional">
		<div class="sixth_block_inner">
			<div class="container-fluid">
				<div class="row">
					<div class="col-xs-12 col-lg-5 fifth_block_colums">
						<div class="sixth_block_head">
							<h3 class="some_about_us wow slideInUp">
								Умный функционал
							</h3>
							<p class="some_about_us_text wow slideInUp">
								Более 40 самых востребованных модулей для интернет-магазинов
							</p>
						</div>
						<div class="slider-nav wow slideInUp">
							<div class="slider_nav_first">
								<h4 class="some_about_us">Модуль “Вместе дешевле”</h4>
								<p class="some_about_us_text">Возможность формировать групповую покупку товаров со скидкой. Удобное управление блоками, возможность формировать неограниченное количество пакетов.</p>
							</div>
							<div class="slider_nav_second">
								<h4 class="some_about_us">Модуль “Блог”</h4>
								<p class="some_about_us_text">Полноценный блог статтей или новостей с управлением и СЕО настройками. Вывод последних записей на главную в виде слайдера.</p>
							</div>
							<div class="slider_nav_three">
								<h4 class="some_about_us">Модуль “Фильтры”</h4>
								<p class="some_about_us_text">Полноценная фильтрация по любым характеристикам товаров. Полностью автоматизированные и настраиваемые фильтры товаров. Ajax подгрузка результатов фильтрации и сортировки.</p>
							</div>
							<div class="slider_nav_four">
								<h4 class="some_about_us">Полноценная карточка товара</h4>
								<p class="some_about_us_text">Настраиваемая карточка товара с характеристиками, модерируемыми отзывами, характеристиками, атрибутами, аксессуарами, покупкой в 1 клик и статичной информацией.</p>
							</div>
							<div class="slider_nav_five">
								<h4 class="some_about_us">Личный кабинет</h4>
								<p class="some_about_us_text">Расширенный личный кабинет с историей заказов, адресами, списком желаний, обменом сообщений и редактированием личной инфорамации.</p>
							</div>
							<div class="slider_nav_six">
								<h4 class="some_about_us">Модуль “Расширенное меню”</h4>
								<p class="some_about_us_text">Расширенное горизонтальное полностью настраиваемое меню с возможностью вывода и расположения любой информации из магазина.</p>
							</div>
							<div class="slider_nav_seven">
								<h4 class="some_about_us">Удобное оформление заказа</h4>
								<p class="some_about_us_text">Удобное понятное одностраничное оформление заказа с возможностью выбора отделений Новой Почты и онлайн оплатой. Множество настроек по управлению оформлением.</p>
							</div>
						</div>
						
					</div>
					<div class="col-xs-12 col-lg-7 slider-for_wrap">
						<div class="slider-for wow slideInUp">
							<div>
								<img src="pingshop/img/new_img/sixth_slide_1.jpg" alt="">
							</div>
							<div>
								<img src="pingshop/img/new_img/pr2.jpg" alt="">
							</div>
							<div>
								<img src="pingshop/img/new_img/pr3.jpg" alt="">
							</div>
							<div>
								<img src="pingshop/img/new_img/pr4.jpg" alt="">
							</div>
							<div>
								<img src="pingshop/img/new_img/pr5.jpg" alt="">
							</div>
							<div>
								<img src="pingshop/img/new_img/pr6.jpg" alt="">
							</div>
							<div>
								<img src="pingshop/img/new_img/pr7.jpg" alt="">
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="seventh_block seven">
		<div class="seventh_block_inner">
			<div class="container-fluid">
				<div class="row">
					<div class="col-xs-12 col-lg-7 wow slideInUp slider-for_wrap">
						<div class="slider-for_second">
							<div>
								<img src="pingshop/img/new_img/pr11.jpg" alt="">
							</div>
							<div>
								<img src="pingshop/img/new_img/pr14.jpg" alt="">
							</div>
							<div>
								<img src="pingshop/img/new_img/pr10.jpg" alt="">
							</div>
							<div>
								<img src="pingshop/img/new_img/pr12.jpg" alt="">
							</div>
							<div>
								<img src="pingshop/img/new_img/pr15.jpg" alt="">
							</div>
							<div>
								<img src="pingshop/img/new_img/pr9.jpg" alt="">
							</div>
						</div>
					</div>
					<div class="col-xs-12 col-lg-5 fifth_block_colums">
						<div class="sixth_block_head">
							<h3 class="some_about_us wow slideInUp">
								СЕРВИСЫ И SEO
							</h3>
							<p class="some_about_us_text wow slideInUp">
								Внедрены популярные внешние сервисы. Произведена подготовка к SEO продвижению.
							</p>
						</div>
						<div class="slider-nav_second wow slideInUp">
							<div class="slider_nav_first">
								<h4 class="some_about_us">Синхронизация с Прайсагрегаторами</h4>
								<p class="some_about_us_text">Внедрен модуль формирования YML файла для отправки в популярные сервисы Prom.ua, Hotline.ua и т.д. Имеет массу настроек для формирования нужного экспорта.</p>
							</div>
							<div class="slider_nav_second wow slideInUp">
								<h4 class="some_about_us">Модуль "Новая Почта"</h4>
								<p class="some_about_us_text">Внедрен модуль Новой Почты при оформлении заказа. Теперь Ваши клиенты могут выбирать город и отделение из базы Новой Почты при оформлении заказа.</p>
							</div>
							<div class="slider_nav_second wow slideInUp">
								<h4 class="some_about_us">Модули платежных сервисов</h4>
								<p class="some_about_us_text">Внедрено сразу 4 популярных платежных сервиса, а также возможность безналичной оплаты и оплаты по факту получения товара. Все модули настраиваемые.</p>
							</div>
							<div class="slider_nav_three wow slideInUp">
								<h4 class="some_about_us">СЕО магазина</h4>
								<p class="some_about_us_text">Внедрена возможность полной СЕО настройки всех страниц, товаров, категорий и других элементов, а полный доступ к файлам магазина дает возможность тонкой настройки для СЕОшника.</p>
							</div>
							<div class="slider_nav_four wow slideInUp">
								<h4 class="some_about_us">Модуль "Карта сайта"</h4>
								<p class="some_about_us_text">Автоматическая генерация карты сайта для поисковых систем. Возможность настройки генератора с указанием генерации нужных страниц.</p>
							</div>
							<div class="slider_nav_four wow slideInUp">
								<h4 class="some_about_us">Модуль "Вход через соцсети"</h4>
								<p class="some_about_us_text">Предоставляется возможность регистрации клиентов через популярные социальные сервисы - Google+ и Facebook. Упрощает процедуру входа клиента.</p>
							</div>
						</div>
						
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="fourth_block">
		<div class="container-fluid">
			<div class="row align_center">
				<div class="col-xs-12 col-lg-6 fourth_block_item">
					<h3 class="fourth_block_head wow slideInUp">Хотите увеличить средний чек и прибыль в несколько раз? </h3>
					<h4 class="fourth_block_head_bold wow slideInUp">Отправьте заявку!</h4>
				</div>
				<div class="col-xs-12 col-lg-6 fourth_block_item">
					
					<div class="columns is-multiline" id="newFormSecond">
						<div class="newForm_inner">
							<form action="POST" @submit.prevent='validateBeforeSubmit' class="flex">
								<div class="column is-12">
									<p class="control has-icon has-icon-right">
											<input name="email" v-model="email" v-validate.initial="'required|email'" :class="{'input': true, 'is-danger': errors.has('email') }" type="email" placeholder="Ваш Email..." autocomplete="off">
											<span class="error_wrap" style="display: none;">
												<i v-show="errors.has('email')" class="fa fa-warning"></i>
												<span v-show="errors.has('email')" class="help is-danger">
													Обязательное поле.
												</span>
											</span>
									</p>
								</div>
								<button>Отправить</button>
							</form>
						</div>
						<div class="resultPlace" v-if="show" style="display: none;">
							<p class="result">Спасибо, данные отправлены! <i class="fa fa-check" aria-hidden="true"></i></p>	
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="sixth_block eight_block">
		<div class="sixth_block_inner">
			<div class="container-fluid">
				<div class="row">
					<div class="col-xs-12 col-lg-5 fifth_block_colums">
						<div class="sixth_block_head">
							<h3 class="some_about_us wow slideInUp">
								РАСШИРЕННАЯ  АНАЛИТИКА
							</h3>
							<p class="some_about_us_text wow slideInUp">
								Если Ваш бизнес измереяем, значит он управляем.
							</p>
							<p class="some_about_us_text center wow slideInUp">
								Предоставлется встроенная в Ваш интернет-магазин аналитика, а также возможность считать прибыль. Дополнительно внедряется аналитика Google Analytics, чтобы Вы могли следить и анализировать эффективность продаж Вашего интернет-магазина.
							</p>
						</div>					
					</div>
					<div class="col-xs-12 col-lg-7 wow slideInUp sixth_block_inner_img">
						<img src="pingshop/img/new_img/eight_block_img.jpg" alt="" class="eigth_block_img">
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="seventh_block nineth_block">
		<div class="seventh_block_inner">
			<div class="container-fluid">
				<div class="row">
					<div class="col-xs-12 col-lg-7 wow slideInUp sixth_block_inner_img">
						<img src="pingshop/img/new_img/nineth_block_img.jpg" alt="" class="eigth_block_img">
					</div>
					<div class="col-xs-12 col-lg-5 fifth_block_colums">
						<div class="sixth_block_head">
							<h3 class="some_about_us wow slideInUp">
								УДОБНАЯ АДМИН-ПАНЕЛЬ
							</h3>
							<p class="some_about_us_text wow slideInUp">
								Управляйте своим бизнесом из любой точки мира с любого устройства
							</p>

							<p class="some_about_us_text center wow slideInUp">
								Удобная админ-панель позволяет осуществлять все необходимые действия по работе с сайтом как Вам, так и Вашим менеджерам: управление товарами, категориями, клиентами и заказами. Все данные надежно хранятся здесь с логированием действий каждого сотрудника.
							</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="sixth_block tenth_block">
		<div class="sixth_block_inner">
			<div class="container-fluid">
				<div class="row">
					<div class="col-xs-12 text_center">
						<h3 class="fourth_block_head wow slideInUp">
							Почему вам выгодно пользоваться
						</h3>
						<h4 class="fourth_block_head_bold wow slideInUp">
							Быстрым стартом?
						</h4>
					</div>
					<div class="col-xs-12 col-sm-4 third_block_item wow slideInUp">
						<div class="row">
							<div class="col-xs-3 col-sm-4 col-lg-3 text_center no_padding_right wow slideInUp">
								<img src="pingshop/img/new_img/ten_block_1.png" alt="">
							</div>
							<div class="col-xs-9 col-sm-8 col-lg-9 no_padding_right">
								<h3 class="second_block_headings wow slideInUp">БЫСТРО</h3>
								<p class="second_block_text wow slideInUp">
									3 дня - и Ваш магазин открыт. Без утомительных согласований.
								</p>
							</div>
						</div>
					</div>
					<div class="col-xs-12 col-sm-4 third_block_item wow slideInUp">
						<div class="row">
							<div class="col-xs-3 col-sm-4 col-lg-3 text_center no_padding_right wow slideInUp">
								<img src="pingshop/img/new_img/ten_block_2.png" alt="">
							</div>
							<div class="col-xs-9 col-sm-8 col-lg-9 no_padding_right">
								<h3 class="second_block_headings wow slideInUp">УДОБНО</h3>
								<p class="second_block_text wow slideInUp">
									Удобно для Вас и удобно для покупателя.
								</p>
							</div>
						</div>
					</div>
					<div class="col-xs-12 col-sm-4 third_block_item wow slideInUp">
						<div class="row">
							<div class="col-xs-3 col-sm-4 col-lg-3 text_center no_padding_right wow slideInUp">
								<img src="pingshop/img/new_img/ten_block_3.png" alt="">
							</div>
							<div class="col-xs-9 col-sm-8 col-lg-9 no_padding_right">
								<h3 class="second_block_headings wow slideInUp">ПРОСТО</h3>
								<p class="second_block_text wow slideInUp">
									Не нужно знать программирование или разбираться в сложных вопросах.
								</p>
							</div>
						</div>
					</div>
					<div class="col-xs-12 col-sm-4 third_block_item wow slideInUp">
						<div class="row">
							<div class="col-xs-3 col-sm-4 col-lg-3 text_center no_padding_right wow slideInUp">
								<img src="pingshop/img/new_img/ten_block_4.png" alt="">
							</div>
							<div class="col-xs-9 col-sm-8 col-lg-9 no_padding_right">
								<h3 class="second_block_headings wow slideInUp">БЕЗОПАСНО</h3>
								<p class="second_block_text wow slideInUp">
									Техническая поддержка, оптимизация для работы с нагрузками - всё уже готово.
								</p>
							</div>
						</div>
					</div>
					<div class="col-xs-12 col-sm-4 third_block_item wow slideInUp">
						<div class="row">
							<div class="col-xs-3 col-sm-4 col-lg-3 text_center no_padding_right wow slideInUp">
								<img src="pingshop/img/new_img/ten_block_5.png" alt="">
							</div>
							<div class="col-xs-9 col-sm-8 col-lg-9 no_padding_right">
								<h3 class="second_block_headings wow slideInUp">ПРИБЫЛЬНО</h3>
								<p class="second_block_text wow slideInUp">
									Быстрый старт продаёт: мы протестировали его на сотнях клиентов.
								</p>
							</div>
						</div>
					</div>
					<div class="col-xs-12 col-sm-4 third_block_item wow slideInUp">
						<div class="row">
							<div class="col-xs-3 col-sm-4 col-lg-3 text_center no_padding_right wow slideInUp">
								<img src="pingshop/img/new_img/ten_block_6.png" alt="">
							</div>
							<div class="col-xs-9 col-sm-8 col-lg-9 no_padding_right">
								<h3 class="second_block_headings wow slideInUp">ВЫГОДНО</h3>
								<p class="second_block_text wow slideInUp">
									Продажи при минимальных  вложениях!
								</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="eleventh_block">
		<h3 class="fourth_block_head text_center wow slideInUp">
			Как начать пользоваться
		</h3>
		<h4 class="fourth_block_head_bold text_center wow slideInUp">
			Быстрым стартом?
		</h4>

		<p class="text_center wow slideInUp">
			<a href="#buy1" class="call_back big ten">Начать пользоваться</a>
		</p>

		<div class="eleventh_block_item text_center wow slideInUp">
			<p class="second_block_text">Знакомимся с Вашим бизнесом. Заключаем договор.</p>
			<p class="orange">1 день</p>
		</div>
		<div class="eleventh_block_item text_center wow slideInUp">
			<p class="second_block_text">Регистрируем домен, переносим на хостинг</p>
			<p class="orange">1 день</p>
		</div>
		<div class="eleventh_block_item text_center wow slideInUp">
			<p class="second_block_text">Утверждаем цветовую гамму, тестируем, обучаем.</p>
			<p class="orange">1 день</p>
		</div>
		<p class="text_center wow slideInUp">
			<a href="#buy1" class="call_back big last">Скачать бесплатно</a>
		</p>
        <div id="buy1" class="modalDialog">
            <div>
                <a href="#close" title="Закрыть" class="close">X</a>
                <!--<img alt="" class="modal_logo" data-pagespeed-url-hash="4123841404" src="img/modal_2_top.png" onerror="this.onerror=null;pagespeed.lazyLoadImages.loadIfVisibleAndMaybeBeacon(this);">
                <h2 class="modal_head">Закажите обратный звонок</h2>
                <p class="modal_text">и мы свяжемся с Вами в ближайшее время</p>
                <form class="fofmCall" action="" enctype="multipart/form-data">
                    <div class="modal-content_items">
                        <input type="text" required="" placeholder="Имя" name="txtname" class="modal_border">
                        <input type="tel" required="" placeholder="Телефон" name="txtphone" class="modal_border" id="modal_tel_call">
                    </div>
                    <span class="discuss_us_modal discuss_us flex align_center"><input type="submit" class="discuss_us_input" value="Заказать звонок" name="btnsend"></span>

                    <p class="success_call_form"></p>
                </form>-->
                <form class="data">
                    <div>
                        <p class="alert alert-info">
                            Для получения файлов интернет-магазина заполните форму. <br>Мы сформируем архив для Вашего домена и отправим Вам со всеми неоходимыми инструкциями. <br>Установка бесплатная!
                        </p>
                    </div>
                    <input type="url" placeholder="Домен (в виде http://test.com)" required>
                    <input type="email" placeholder="Email" required>
                    <input class="submit-yandex call_back big bubble left wow" type="submit" style="margin:0 auto;visibility: visible;" value="<?php echo FAST_DOWNLOAD; ?>">

                    <div class="success_call_form"></div>
                </form>
                <div class="info" style="display:none">
                    <div class="alert alert-info">
                        Спасибо! Архив уже формируется ожидайте его на своей почте в ближайшее время!
                    </div>
                </div>
            </div>
        </div>
	</div>
    <div id="demomodal" class="modalDialog">
        <div>
            <a href="#close" title="Закрыть" class="close">X</a>
            <h2>Демо front-office:</h2>
            <a href="https://hotshop.pinguin-studio.com.ua/" target="_blank" id="see_front">Смотреть</a>
            <h2>Демо back-office:</h2>
            <div class="alert alert-info">
                <p>
                    <b>Логин: demo@demo.com</b></p><br/>
                </p>
                <p>
                    <b>Пароль: demodemo</b><br/>
                </p>
            </div>
            <a href="https://hotshop.pinguin-studio.com.ua/root" target="_blank" id="see_front">Смотреть</a>
        </div>
    </div>
	<footer>
		<div class="container-fluid">
			<div class="row">
				<div class="col-xs-12 col-sm-6 col-md-6 display_inline_block wow slideInUp">
					<h3 class="footer_head"><span>Контакты</span></h3>
					<div class="row footer_contacts_line">
						<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 no_padding">
							<p class="footer_phone">Телефон:</p>
						</div>
						<div class="col-xs-9 col-sm-9 col-md-9 col-lg-9 no_padding">
							<span><a href="tel:0687288989" class="footer_phone_item">(068) 728-89-89</a>, <a href="tel:0953397379">(095) 339-73-79</a></span>
						</div>
					</div>
					<div class="row footer_contacts_line">
						<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 no_padding">
							<p class="email">Mail:</p>
						</div>
						<div class="col-xs-9 col-sm-9 col-md-9 col-lg-9 no_padding">
						 	<span><a href="mailto:zakaz@pinguin-studio.com.ua" class="footer_phone_item">zakaz@pinguin-studio.com.ua</a></span>
						</div>
					</div>
					 <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 no_padding">
					 	<ul class="footer_social flex flex_wrap"  style="display: none;">
							<li><a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
							<li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
							<li><a href="#"><i class="fa fa-youtube" aria-hidden="true"></i></a></li>
							<li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
							<li><a href="#"><i class="fa fa-behance" aria-hidden="true"></i></a></li>
						</ul>
					 </div>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-6 display_inline_block wow slideInUp">
					<h3 class="footer_head"><span>Коротко о нас для роботов</span></h3>
					<p class="footer_text_for_robots">Pinguin-Studio.com.ua - Создание сайтов в Днепре. Разработка интернет-магазинов PINGUIN-STUDIO. Cоздание сайтов в Днепре, разработка интернет-магазинов в Днепре. Разработка сайтов, раскрутка, продвижение. © PINGUIN-STUDIO, 2009-2018</p>
				</div>	
			</div>
		</div>
	</footer>
<!-- Latest compiled and minified JavaScript -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="/js/jquery.mask.min.js"></script>
    <script src="pingshop/js/slick.min.js"></script>
    <script src="pingshop/js/parallax.js"></script>
    <script src="pingshop/js/wow.min.js"></script>
    <script src="pingshop/js/vue.js"></script>
    <script src="pingshop/js/vee-validate.js"></script>
    <script src="pingshop/js/axios.min.js"></script>
    <script src="pingshop/js/main.js"></script>
    <script src="pingshop/js/buy.js"></script>
<!-- <script src="js/preloader_home.js"></script> -->
<!-- BEGIN JIVOSITE CODE {literal} -->
<script type='text/javascript'>
(function(){ var widget_id = 'KrP5X7jF78';var d=document;var w=window;function l(){
var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = '//code.jivosite.com/script/widget/'+widget_id; var ss = document.getElementsByTagName('script')[0]; ss.parentNode.insertBefore(s, ss);}if(d.readyState=='complete'){l();}else{if(w.attachEvent){w.attachEvent('onload',l);}else{w.addEventListener('load',l,false);}}})();</script>
<!-- {/literal} END JIVOSITE CODE -->
</body>
</html>